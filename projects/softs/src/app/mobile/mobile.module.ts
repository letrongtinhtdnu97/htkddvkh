import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DynamicDialogModule } from 'primeng/dynamicdialog'
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { GalleriaModule } from 'primeng/galleria';
import { InplaceModule } from 'primeng/inplace';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { LightboxModule } from 'primeng/lightbox';
import { ListboxModule } from 'primeng/listbox';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { OrderListModule } from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PasswordModule } from 'primeng/password';
import { PickListModule } from 'primeng/picklist';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { RippleModule } from 'primeng/ripple';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SidebarModule } from 'primeng/sidebar';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SplitButtonModule } from 'primeng/splitbutton';
import { StepsModule } from 'primeng/steps';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TerminalModule } from 'primeng/terminal';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';

import { VirtualScrollerModule } from 'primeng/virtualscroller';

import { CountryService } from '../demo/service/countryservice';
import { CustomerService } from '../demo/service/customerservice';
import { EventService } from '../demo/service/eventservice';
import { IconService } from '../demo/service/iconservice';
import { NodeService } from '../demo/service/nodeservice';
import { PhotoService } from '../demo/service/photoservice';
import { ProductService } from '../demo/service/productservice';
import { AccountService } from './services/account.service';
import { DeviceService } from './services/device.service';
import { BienbanService } from './services/bienban.service';
import { BreadcrumbService } from '../core/services/breadcrumb.service';
import { MenuService } from '../app.menu.service';
import { EncryptService } from './services/encrypt.service';

import { DeviceAddComponent } from './components/device-add/device-add.component';
import { DeviceEditComponent } from './components/device-edit/device-edit.component';
import { DeviceListComponent } from './components/device-list/device-list.component';
import { AccountListComponent } from './components/account-list/account-list.component';
import { AccountAddComponent } from './components/account-add/account-add.component';
import { AccountEditComponent } from './components/account-edit/account-edit.component';

import { BbCmisListComponent } from './components/bb-cmis-list/bb-cmis-list.component';
import { BbCmisAddComponent } from './components/bb-cmis-add/bb-cmis-add.component';
import { BbCmisEditComponent } from './components/bb-cmis-edit/bb-cmis-edit.component';
import { BbCmisVattuComponent } from './components/bb-cmis-add/bb-cmis-vattu/bb-cmis-vattu.component';
import { BbCmisThietBiComponent } from './components/bb-cmis-add/bb-cmis-thiet-bi/bb-cmis-thiet-bi.component';
import { BbCmisTiComponent } from './components/bb-cmis-add/bb-cmis-ti/bb-cmis-ti.component';
import { BbCmisTuComponent } from './components/bb-cmis-add/bb-cmis-tu/bb-cmis-tu.component';
import { BaoCaoTongHopVatTuComponent } from './components/bao-cao-tong-hop-vat-tu/bao-cao-tong-hop-vat-tu.component';
import { TaoBienBanComponent } from './components/tao-bien-ban/tao-bien-ban.component';
import { QuyettoanVattuComponent } from './components/quyettoan-vattu/quyettoan-vattu.component';


@NgModule({
  declarations: [
    DeviceAddComponent,
    DeviceEditComponent,
    DeviceListComponent,
    AccountListComponent,
    AccountAddComponent,
    AccountEditComponent,
    BbCmisListComponent,
    BbCmisAddComponent,
    BbCmisEditComponent,
    BbCmisVattuComponent,
    BbCmisThietBiComponent,
    BbCmisTiComponent,
    BbCmisTuComponent,
    BaoCaoTongHopVatTuComponent,
    TaoBienBanComponent,
    QuyettoanVattuComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    FullCalendarModule,
    GalleriaModule,
    InplaceModule,
    InputNumberModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    RippleModule,
    ScrollPanelModule,
    SelectButtonModule,
    SidebarModule,
    SlideMenuModule,
    SliderModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    VirtualScrollerModule,
    DynamicDialogModule,
    
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    CountryService, CustomerService, EventService, IconService, NodeService,
    PhotoService, ProductService, AccountService, DeviceService,
    BienbanService, MenuService, BreadcrumbService, EncryptService
  ]
})
export class MobileModule { }
