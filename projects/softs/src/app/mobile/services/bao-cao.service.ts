import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/services/config.service';
import { Observable } from 'rxjs/internal/Observable';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { RP_VATTU } from '../models/report.model';
@Injectable({
  providedIn: 'root'
})
export class BaoCaoService {
  protected url = ConfigService.config.url;

  constructor(private http: HttpClient) { }

  reportVatTu(body: any):Observable<any> {
    return this.http.post(this.url.udht + 'reportVattu',body)
  }
  reportKhang(body: any):Observable<any> {
    return this.http.post(this.url.udht + 'reportKhang',body)
  }
  reportCongto(body: any):any {
    return this.http.post(this.url.udht + 'reportCongto',body)
  }
  showReportQtoanCto(body, dateStart, dateEnd,sizeMentor):void {
    const title = `QUYẾT TOÁN VẬT TƯ \r\n Từ ngày ${dateStart ? dateStart : ''} đến ${dateEnd ? dateEnd : ''}`;
      const header = ["STT", "TÊN THIẾT BỊ VẬT TƯ", "ĐVT", "SỐ LƯỢNG DỰ TOÁN", "SỐ LƯỢNG THỰC TẾ"]

      const data = body;
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet('BM4.2');
      

      let titleRow = worksheet.addRow([title]);
      titleRow.font = { name: 'Time New Roman',  size: 14, bold: true };
      titleRow.height = 30;
      titleRow.alignment = {horizontal: 'center', vertical:'middle',wrapText: true}
      worksheet.addRow([]);
      worksheet.mergeCells('A1:E2');
      
      let headerRow = worksheet.addRow(header);
      headerRow.eachCell((cell, number) => {
        
        cell.font = { name: 'Time New Roman',  size: 14, bold: true };
        
        cell.alignment = {
          horizontal: 'center',
          vertical: 'middle'
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      });
      let fontText = { name: 'Time New Roman',  size: 13};
     
      
      let result = Object.keys(sizeMentor).map((key) => [(key), sizeMentor[key]]);
      let colNumber = 3
      result.forEach((d) => {
        
        let row1 = worksheet.addRow([d[0]]);
        row1.alignment = {horizontal:'left', vertical:'middle'}
        row1.font = {bold: true, size: 14, name: 'Time New Roman'}
        if(d[1].index === 0) {
          worksheet.mergeCells(`A${d[1].index + colNumber +  1}:E${d[1].index + 4}`);

        } else {
          worksheet.mergeCells(`A${d[1].index + colNumber +  2}:E${d[1].index + colNumber +  2}`);
        }
        
        data.forEach((element: RP_VATTU,index) => {
          let arrCell = [index + 1, element.MA_VTU, element.TEN_VTU,element.DVI_TINH,element.SO_LUONG]
          
          if(element.TEN_NVIEN_TCONG === d[0]) {
            let row2 = worksheet.addRow(arrCell);
            console.log(123123);
          }
        });
       
      });
      worksheet.getColumn('A').width = 10;
      

      worksheet.getColumn('B').width = 20;
     

      worksheet.getColumn('C').width = 45;

      worksheet.getColumn('D').width = 30;

      worksheet.getColumn('E').width = 30;
      
      worksheet.addRow([])
      worksheet.addRow(['',`Tổng cộng: ${data.length} `]).font = { name: 'Time New Roman',  size: 14, bold: true };
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, 'BM4.1.xlsx');
      });
  }
  showReportCtoPhanBo(body, dateStart, dateEnd,sizeMentor):void {
    const title = `DANH SÁCH CÔNG TƠ ĐƯỢC PHÂN BỔ \r\n Từ ngày ${dateStart ? dateStart : ''} đến ${dateEnd ? dateEnd : ''}`;
      const header = ["STT", "MÃ CÔNG TƠ", "SÔ NO", "LOẠI CÔNG TƠ", "SỐ PHA", "CẤP CHÍNH XÁC",'DÒNG ĐIỆN', 'ĐIỆN ÁP', 'NGÀY PHÂN BỔ']

      const data = body;
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet('BM3.2');
      

      let titleRow = worksheet.addRow([title]);
      titleRow.font = { name: 'Time New Roman',  size: 14, bold: true };
      titleRow.height = 30;
      titleRow.alignment = {horizontal: 'center', vertical:'middle',wrapText: true}
      worksheet.addRow([]);
      worksheet.mergeCells('A1:I2');
      
      let headerRow = worksheet.addRow(header);
      headerRow.eachCell((cell, number) => {
        
        cell.font = { name: 'Time New Roman',  size: 14, bold: true };
        
        cell.alignment = {
          horizontal: 'center',
          vertical: 'middle'
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      });
      let fontText = { name: 'Time New Roman',  size: 13};
     
      
      let result = Object.keys(sizeMentor).map((key) => [(key), sizeMentor[key]]);
      let colNumber = 3
      result.forEach((d) => {
        
        let row1 = worksheet.addRow([d[0]]);
        row1.alignment = {horizontal:'left', vertical:'middle'}
        row1.font = {bold: true, size: 14, name: 'Time New Roman'}
        if(d[1].index === 0) {
          worksheet.mergeCells(`A${d[1].index + colNumber +  1}:I${d[1].index + 4}`);

        } else {
          worksheet.mergeCells(`A${d[1].index + colNumber +  2}:I${d[1].index + colNumber +  2}`);
        }
        
        data.forEach((element: RP_VATTU,index) => {
          let arrCell = [index + 1, element.MA_VTU, element.TEN_VTU,element.DVI_TINH,element.SO_LUONG]
          
          if(element.TEN_NVIEN_TCONG === d[0]) {
            let row2 = worksheet.addRow(arrCell);
            console.log(123123);
          }
        });
       
      });
      worksheet.getColumn('A').width = 10;
      

      worksheet.getColumn('B').width = 20;
     

      worksheet.getColumn('C').width = 45;

      worksheet.getColumn('D').width = 20;

      worksheet.getColumn('E').width = 20;
      worksheet.getColumn('F').width = 20;
      worksheet.getColumn('G').width = 20;
      worksheet.getColumn('H').width = 20;
      worksheet.getColumn('I').width = 20;
      worksheet.addRow([])
      worksheet.addRow(['',`Tổng cộng: ${data.length} `]).font = { name: 'Time New Roman',  size: 14, bold: true };
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, 'BM2.1.xlsx');
      });
  }
  showReportKhDThicong(body, dateStart, dateEnd,sizeMentor):void {
    const title = `DANH SÁCH KHÁCH HÀNG ĐƯỢC DUYỆT THI CÔNG \r\n Từ ngày ${dateStart ? dateStart : ''} đến ${dateEnd ? dateEnd : ''}`;
      const header = ["STT", "MÃ YÊU CẦU", "TÊN KHÁCH HÀNG", "ĐỊA CHỈ", "ĐIỆN THOẠI", "NGÀY PHÂN CÔNG",'NGÀY DUYỆT PHÂN CÔNG', 'NGÀY THI CÔNG']

      const data = body;
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet('BM1.2');
      

      let titleRow = worksheet.addRow([title]);
      titleRow.font = { name: 'Time New Roman',  size: 14, bold: true };
      titleRow.height = 30;
      titleRow.alignment = {horizontal: 'center', vertical:'middle',wrapText: true}
      worksheet.addRow([]);
      worksheet.mergeCells('A1:H2');
      
      let headerRow = worksheet.addRow(header);
      headerRow.eachCell((cell, number) => {
        
        cell.font = { name: 'Time New Roman',  size: 14, bold: true };
        
        cell.alignment = {
          horizontal: 'center',
          vertical: 'middle'
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      });
      let fontText = { name: 'Time New Roman',  size: 13};
     
      
      let result = Object.keys(sizeMentor).map((key) => [(key), sizeMentor[key]]);
      let colNumber = 3
      result.forEach((d) => {
        
        let row1 = worksheet.addRow([d[0]]);
        row1.alignment = {horizontal:'left', vertical:'middle'}
        row1.font = {bold: true, size: 14, name: 'Time New Roman'}
        if(d[1].index === 0) {
          worksheet.mergeCells(`A${d[1].index + colNumber +  1}:H${d[1].index + 4}`);

        } else {
          worksheet.mergeCells(`A${d[1].index + colNumber +  2}:H${d[1].index + colNumber +  2}`);
        }
        
        data.forEach((element: RP_VATTU,index) => {
          let arrCell = [index + 1, element.MA_VTU, element.TEN_VTU,element.DVI_TINH,element.SO_LUONG]
          
          if(element.TEN_NVIEN_TCONG === d[0]) {
            let row2 = worksheet.addRow(arrCell);
          }
        });
       
      });
      worksheet.getColumn('A').width = 10;
      

      worksheet.getColumn('B').width = 20;
     

      worksheet.getColumn('C').width = 45;

      worksheet.getColumn('D').width = 20;
      //worksheet.getColumn('D').alignment = {horizontal:'center', vertical:'middle'}

      worksheet.getColumn('E').width = 20;  
      worksheet.getColumn('F').width = 30;
      worksheet.getColumn('G').width = 30;
      worksheet.getColumn('H').width = 30;
      worksheet.addRow([])
      worksheet.addRow(['',`Tổng cộng: ${data.length} `]).font = { name: 'Time New Roman',  size: 14, bold: true };
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, 'BM1.2.xlsx');
      });
  }
  showReportVattu(body: any, dateStart: String, dateEnd: String ,sizeMentor):void {
      const title = `TỔNG HỢP VẬT TƯ ĐƯỢC PHÂN BỔ \r\n Từ ngày ${dateStart ? dateStart : ''} đến ${dateEnd ? dateEnd : ''}`;
      const header = ["STT", "MÃ VẬT TƯ", "TÊN VẬT TƯ", "ĐƠN VỊ TÍNH", "SỐ LƯỢNG"]
      const data = body;
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet('BM2.1');
      

      let titleRow = worksheet.addRow([title]);
      titleRow.font = { name: 'Time New Roman',  size: 14, bold: true };
      titleRow.height = 30;
      titleRow.alignment = {horizontal: 'center', vertical:'middle',wrapText: true}
      worksheet.addRow([]);
      worksheet.mergeCells('A1:E2');
      
      let headerRow = worksheet.addRow(header);
      headerRow.eachCell((cell, number) => {
        
        cell.font = { name: 'Time New Roman',  size: 14, bold: true };
        
        cell.alignment = {
          horizontal: 'center',
          vertical: 'middle'
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      });
      let fontText = { name: 'Time New Roman',  size: 13};
      // let nameNgPcong = 'Tên nhân viên: ' + data[0].TEN_NVIEN_TCONG
      
      // let col4 = worksheet.getCell('A4')
      // col4.value  = nameNgPcong
      // col4.font = { name: 'Time New Roman',  size: 14, bold: true };
      // //col4.style = {alignment:{horizontal:'left', vertical:'middle'}}
      // worksheet.mergeCells('A4:E4');
      
      let result = Object.keys(sizeMentor).map((key) => [(key), sizeMentor[key]]);
      let colNumber = 3
      result.forEach((d) => {
        
        let row1 = worksheet.addRow([d[0]]);
        row1.alignment = {horizontal:'left', vertical:'middle'}
        row1.font = {bold: true, size: 14, name: 'Time New Roman'}
        if(d[1].index === 0) {
          worksheet.mergeCells(`A${d[1].index + colNumber +  1}:E${d[1].index + 4}`);

        } else {
          worksheet.mergeCells(`A${d[1].index + colNumber +  2}:E${d[1].index + colNumber +  2}`);
        }
        
        data.forEach((element: RP_VATTU,index) => {
          let arrCell = [index + 1, element.MA_VTU, element.TEN_VTU,element.DVI_TINH,element.SO_LUONG]
          
          if(element.TEN_NVIEN_TCONG === d[0]) {
            let row2 = worksheet.addRow(arrCell);
            console.log(123123);
          }
        });
          //let row = worksheet.addRow(d[0].size);
          //let col5 = worksheet.getCell(4 + index + 1)
          //col5.value  = d[0];
          //worksheet.mergeCells(`A${4+index+1}:E${4+index+1}`);
          //col5.font = { name: 'Time New Roman',  size: 14, bold: true };
        //let row = worksheet.addRow(d);
        // let arrCell = [index + 1, d.MA_VTU, d.TEN_VTU,d.DVI_TINH,d.SO_LUONG]

        // let row = worksheet.addRow(arrCell);
        // row.font = fontText;
        
        // //row.getCell(index).alignment = {horizontal:'center', vertical:'middle'}
        // console.log(d.TEN_NVIEN_TCONG);
        // if(d.TEN_NVIEN_TCONG === nameNgPcong) {
        //   let col5 = worksheet.getCell(4 + index + 1)
        //   nameNgPcong = d.TEN_NVIEN_TCONG;
        //   col5.value  = nameNgPcong
        //   worksheet.mergeCells(`A${4+index+1}:E${4+index+1}`);
        //   col5.font = { name: 'Time New Roman',  size: 14, bold: true };
        // }

      });
      worksheet.getColumn('A').width = 10;
      

      worksheet.getColumn('B').width = 20;
     

      worksheet.getColumn('C').width = 45;

      worksheet.getColumn('D').width = 20;

      worksheet.getColumn('E').width = 20;
      
      worksheet.addRow([])
      worksheet.addRow(['',`Tổng cộng: ${data.length} `]).font = { name: 'Time New Roman',  size: 14, bold: true };
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, 'BM2.1.xlsx');
      });
  }
}
