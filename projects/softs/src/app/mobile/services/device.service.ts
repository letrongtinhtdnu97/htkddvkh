import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../config/services/config.service';

const option = {
  'Content-Type': 'application/json'
};
@Injectable()
export class DeviceService {
  protected url = ConfigService.config.url;
  constructor(private http: HttpClient) { }

  

  GetDevice(body: any) {
    return this.http.post(this.url.kddvkd + 'getAllDevice',body)
  }

  AddDevice(body: any) {
    return this.http.post(this.url.kddvkd + 'addDevice',body)
  }

  UpdateDevice(body: any) {
    return this.http.post(this.url.kddvkd + 'updateDevice',body)
  }

  DeleteDevice(body: any) {
    return this.http.post(this.url.kddvkd + 'deleteDevice',body)
  }
  GetDeviceNotConnectAccount(body: any) {
    return this.http.post(this.url.kddvkd + 'getAllCategoryDevice',body)
  }
  
}
