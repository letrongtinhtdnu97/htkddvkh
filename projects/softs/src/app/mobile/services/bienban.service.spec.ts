import { TestBed } from '@angular/core/testing';

import { BienbanService } from './bienban.service';

describe('BienbanService', () => {
  let service: BienbanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BienbanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
