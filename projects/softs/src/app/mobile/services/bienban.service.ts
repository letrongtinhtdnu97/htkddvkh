import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/services/config.service';

@Injectable()
export class BienbanService {
  protected url = ConfigService.config.url;

  constructor(private http: HttpClient) { }

  getStatus(): any {
    const data = [
      {
        ID: 0,
        NAME: 'Mới giao',
        BBGIAO: true,
        BBNHAN: false
      },
      {
        ID: 1,
        NAME: 'Nhận mobile',
        BBGIAO: true,
        BBNHAN: false
      },
      // {
      //   ID: 2,
      //   NAME: 'Đang điều tra',
      //   BBGIAO: true,
      //   BBNHAN: false
      // },
      {
        ID: 3,
        NAME: 'Đã chốt',
        BBGIAO: true,
        BBNHAN: false
      },
      {
        ID: 4,
        NAME: 'Nhận CMIS',
        BBGIAO: false,
        BBNHAN: true
      }
    ];
    return data;
  }
  //start tinhlt - 23022021
  getBienBanGiaoAndKhachHang(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBienBanGiaoAndKhachHang', param);
  }
  getBienBanGiaoPrint(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBienBanGiaoPrint', param);
  }
  getBienBanNhanAndKhachHang(param: any):any {
    return this.http.post(this.url.dtttkh + 'getBienBanNhanAndKhachHang', param);
  }
  updateBienBanGiao(param: any): any {
    return this.http.post(this.url.dtttkh + 'updateBienBanGiao', param);
  }
  //end

  //notify 
  notificationForeground(param: any): any {
    return this.http.post(this.url.dtttkh + 'pushNotificationToDevice', param);
  }

  pushNoticationToDelete(param: any) {
    return this.http.post(this.url.dtttkh + 'pushNoticationToDelete',param);
  }


  //start tinhlt - 23032021
  searchBBanGiaoByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'searchBBanGiaoByCondition', param);
  }
  exportExcel(param: any): any {
    return this.http.post(this.url.dtttkh + 'exportExcel', param, { responseType: 'arraybuffer' });
  }

  exportExcelDetail(param: any): any {
    return this.http.post(this.url.dtttkh + 'exportExcelDetail', param, { responseType: 'arraybuffer' });
  }

  getUserByIDs(param: any): any {
    return this.http.post(this.url.qtht + 'getUserByIDs', param);
  }
  getIDUserGiaoBBan(param: any):any {
    return this.http.post(this.url.dtttkh + 'getIDUserGiaoBBan', param);
  }
  //end
  getSoGCSByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getSoGCSByCondition', param);
  }
  getTramByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getTramByCondition', param);
  }

  getCustomersByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getCustomersByCondition', param);
  }
  
  giaoBBanDTKH(param: any): any {
    return this.http.post(this.url.dtttkh + 'giaoBBanDTKH', param);
  }

  getBBanGiaoByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBBanGiaoByCondition', param);
  }

  getDetailBBanDTKH(param: any): any {
    return this.http.post(this.url.dtttkh + 'getDetailBBanDTKH', param);
  }

  getBBanNhanByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBBanNhanByCondition', param);
  }

  getBBanDangTHienByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBBanDangTHienByCondition', param);
  }

  getBBanChotTHienByCondition(param: any): any {
    return this.http.post(this.url.dtttkh + 'getBBanChotTHienByCondition', param);
  }
  getEmployeeByID(param: any) {
    return this.http.post(this.url.dtttkh + 'getBBanChotTHienByCondition', param);
  }
  getCTietBBanChotTHien(param: any): any {
    return this.http.post(this.url.dtttkh + 'getCTietBBanChotTHien', param);
  }
  getKhangHangDangGiao(param: any): any {
    return this.http.post(this.url.dtttkh + 'getKhangHangDangGiao', param);
  }
  nhanBBanDTKHVaoCMIS(param: any): any {
    return this.http.post(this.url.dtttkh + 'nhanBBanDTKHVaoCMIS', param);
  }
  
  deleteBienBan(param: any): any {
    return this.http.post(this.url.dtttkh + 'deleteBienBan', param);
  }
  deleteBienBans(param: any): any {
    return this.http.post(this.url.dtttkh + 'deleteBienBans', param);
  }
  getTThaiBienBan(trangthai: number): string {
    switch (trangthai) {
      case 0: {
        return 'Mới giao';
      }
      case 1: {
        return 'Nhận mobile';
      }
      case 2: {
        return 'Đang điều tra';
      }
      case 3: {
        return 'Đã chốt';
      }
      case 4: {
        return 'Nhận CMIS';
      }
    }
  }
 
  getTThaiKhachHang(tthaibban: number, tthaikhang: number): string {
    switch (tthaibban) {
      case 2: {
        if (tthaikhang === 0) {
          return 'Chưa điều tra';
        } else if (tthaikhang === 1) {
          return 'Không đổi';
        }
        return 'Thay đổi';
      }
      case 3: {
        if (tthaikhang === 0) {
          return 'Chưa điều tra';
        } else if (tthaikhang === 1) {
          return 'Không đổi';
        }
        return 'Thay đổi';
      }
      case 4: {
        if (tthaikhang === 0) {
          return 'Chưa nhận CMIS';
        }
        return 'Nhận CMIS';
      }
    }
  }
}
