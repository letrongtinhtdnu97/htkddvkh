import { TestBed } from '@angular/core/testing';

import { CmisService } from './cmis.service';

describe('CmisService', () => {
  let service: CmisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CmisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
