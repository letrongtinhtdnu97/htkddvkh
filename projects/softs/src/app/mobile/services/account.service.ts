import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/services/config.service';

import { Account,Profile,CategoryDevice } from '../models/account.model';

const option = {
  'Content-Type': 'application/json'
};
@Injectable()
export class AccountService {
  protected url = ConfigService.config.url;
  constructor(private http: HttpClient) { }

  GetAll(body: any) {
    return this.http.post( this.url.kddvkd + 'getAllAccount',body)
  }
  AddAccount(body: any) {
    return this.http.post( this.url.kddvkd + 'registerAccount',body)
  }
  UpdateAccount(body: any) {
    return this.http.post( this.url.kddvkd + 'updateAccount',body)
  }

  DeleteAccount(body: any) {
    return this.http.post( this.url.kddvkd + 'deleteAccount',body)
  }
}
