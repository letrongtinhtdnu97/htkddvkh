import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/services/config.service';
@Injectable({
  providedIn: 'root'
})
export class CmisService {
  protected url = ConfigService.config.url;
 
  constructor(
    private http: HttpClient,
  ) { }

  dsHsoToCmis(body:any):any {
    return this.http.post( this.url.ktdlcmis + 'dsHinhKy', body)
  }
  dsYeuCauThiCong(body: any) {
    return this.http.post( this.url.ktdlcmis + 'dsYeuCauThiCong', body)
  }
  dSBoPhan(body: any) {
    return this.http.post(this.url.ktdlcmis + 'DSBoPhan', body)
  }

  dSNhanVienToBoPhan(body: any) {
    return this.http.post(this.url.ktdlcmis + 'DSNhanVienToBoPhan', body)
  }
  dsDuToanVatTuKhachHang(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsDuToanVatTuKhachHang',body)
  }
  dsCongTo(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsCongTo', body)
  }

  dsHinhKhaoSat(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsHinhKhaoSat',body)
  }

  dsKHKhaoSat(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsKHKhaoSat',body)
  }
  dsHsoTi(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsHsoTi',body)
  }
  dsHsoTu(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsHsoTu',body)
  }
  dsVatTu(body: any) {
    return this.http.post(this.url.ktdlcmis + 'dsVatTu',body)
  }
  insertTienXuLy(body: any) {
    return this.http.post(this.url.udht + 'insertTienXuLy',body)
  }
  getAllBBanPhanCongToDvi(body: any) {
    return this.http.post( this.url.udht + 'getAllBBanPhanCongToDvi',body)
  }
}
