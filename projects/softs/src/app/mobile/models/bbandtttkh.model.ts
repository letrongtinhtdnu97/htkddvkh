import { Khachhang } from './Khachhang.model';

export class Bbandtttkh {
    SO_BIEN_BAN: number;
    MA_QLKH: number;
    NV_TH?: string;
    NV_GIAOBB?:string;
    MA_NVKS: number;
    MA_DVIQLY: string;
    NGAY_GIAO: Date;
    NGAY_NHAN: Date;
    TSO_KHANG: number;
    TSO_KHTD: number;
    TRANG_THAI: number;
    LST_KHANG?: Khachhang[];
}
