export class Account {
    ACCOUNT_ID?: Number;  
    
    ACCOUNT_NAME: String;
    PASSWORD: String;
    PASSWORD_SALT: String ;
    PASSWORD_ALGORITHM: String;
    FULL_NAME: String;
    EMAIL: String ;
    MOBILE: String ;
    DEVICE_ID?: Number;
    DEVICE_NAME?: String;
    EMPLOYEE_ID?: any ;
    ORG_CODE: String ;
    STATUS: Number ;
    CREATED_ON?: any ;
    CREATED_BY?: String;
    MODIFIED_ON?: any ;
    MODIFIED_BY?: String;
}

export class Profile {
    maUser?: number;
    nameDevice?: string;
    userAccount?: string;
    userName?: string;
    email?: string;
    phone?: string;
    password?: string;
    status?: number;
    createOn?: any;
    modifyOn?: any;
    createBy?: string;
    modifyBy?: string;
}
export class CategoryDevice {
    nameDevice?: string;
    maDevice?: number;
    maImei?: string;
    serialNumber?: string;
}

export class Employee {
    DEPT_ID?: number;
    DEPT_NAME?: string;
    EMAIL?: string;
    EMPLOYEE_CODE?: string;
    EMPLOYEE_ID?: number;
    EMPLOYEE_NAME?: string;
    GENDER?: number;
    HIRE_DATE?: any;
    MOBILE?: string;
    ORG_CODE?: string;
    ORG_ID?: number;
    ORG_NAME?: string;
    MADVI_KD?: string;
}

export class USER {
    EMAIL?: string
    EMPLOYEE_ID?: number
    FULL_NAME?: string
    ORG_CODE?: string
    STATUS?: number
    USER_ID?: number
    USER_NAME?: string
} 
