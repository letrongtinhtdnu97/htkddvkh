export class KHKhaoSat {
    MA_DVIQLY?: String
    MA_YCAU_KNAI?: String    
    TEN_KHANG?: String    
    SO_NHA?: String    
    DUONG_PHO?: String  
    DIA_CHI?: String   
    ID_DIA_CHINH?: String   
    TEN_DIACHINH?: String   
    DIEN_THOAI?: String    
    DTHOAI_CCAP_DVU?: String    
    MA_DDO_DDIEN?: Number    
    CONG_SUAT?: Number    
    MDICH_SHOAT?: Number   
    SO_PHA?: Number    
    DDAN_HOSO?: String   
    MA_TRAM?: String    
    LO_TRUNGTHE?: String   
    SO_TRU?: String  
    VTRI_CTO?: Number    
    CSUAT_THUCTE?: Number    
    DUYET?: Number   
    TOADO_X?: String    
    TOADO_Y?: String   
    LOAI_SOHUU?: Number  
    MA_CLOAI?: String 
}