export class Device {
    DEVICE_ID: String;
    DEVICE_NAME: String;
    FULL_NAME?: String;
    ACCOUNT_NAME?: String;
    IMEI: String ;
    SERY_SIM: String ;
    ORG_CODE: String ;
    TOKEN?: String ;
    ACTIVE?: Number ;
    CREATED_ON?: String;
    CREATED_BY?: String ;
    MODIFIED_ON?: String;
    MODIFIED_BY?: String ;
}

export class UserDevice {
    maDevice?: number;
    maUser?: number;
    nameDevice?: string;
    maImei?: string;
    serialNumber?: string;
    userAccount?: string;
    status?: number;
    createOn?: any;
    modifyOn?: any;
    createBy?: string;
    modifyBy?: string;
}
