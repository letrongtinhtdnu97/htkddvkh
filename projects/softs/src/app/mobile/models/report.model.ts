export class RP_VATTU  {
    MA_DVIQL?: string;
    SO_BBAN_TCONG?: string;
    MA_VTU?: string;
    TEN_VTU?: string;
    SO_LUONG?: string;
    DON_GIA?: string;
    DVI_TINH?: string;
    TINH_TIEN?: string;
    MA_NVIEN_PCONG?: string; 
    MA_NVIEN_TCONG?: string; 
    NGAY_PCONG?: string;
    TEN_NVIEN_PCONG?: string;
    TEN_NVIEN_TCONG?: string;
    NGAY_SUA?: string;
    NGAY_TAO?: string;
    MA_YCAU_KNAI?: string;
}