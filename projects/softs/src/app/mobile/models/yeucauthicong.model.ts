export class YCThiCong {
    MA_DVIQLY?: String
    MA_YCAU_KNAI?: String
    MA_KHANG?: String
    TEN_KHANG?: String
    DTHOAI?: String
    TEN_NGUOIYCAU?: String
    DCHI_NGUOIYCAU?: String
    DTHOAI_DD?: String 
    NGAY_BDAU?: String 
    NGAY_KTHUC?: String 
    MA_BPHAN_NHAN?: String 
    MA_NVIEN_NHAN?: String 
    TEN_LOAIHD?: String
    TEN_VTRI?: String
    
}

export interface IYCThiCong {
    MA_DVIQLY?: String
    MA_YCAU_KNAI?: String
    MA_KHANG?: String
    TEN_KHANG?: String
    DTHOAI?: String
    TEN_NGUOIYCAU?: String
    DCHI_NGUOIYCAU?: String
    DTHOAI_DD?: String 
    NGAY_BDAU?: String 
    NGAY_KTHUC?: String 
    MA_BPHAN_NHAN?: String 
    MA_NVIEN_NHAN?: String 
    TEN_LOAIHD?: String
    TEN_VTRI?: String
}