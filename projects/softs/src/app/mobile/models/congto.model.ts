export class CongTo {
    MA_CTO?: String
    SO_CTO?: String 
    NAM_SX?: String  
    MA_CLOAI?: String   
    SO_HUU?: String  
    MA_DVI_SD?: String    
    SLAN_SDUNG?: String   
    TTRANG_KD?: String   
    MA_BDONG?: String   
    NGAY_BDONG?: String  
    SO_BBAN?: String    
    NGAY_KDINH?: String   
    SLAN_IN?: String    
    MTEM_KD?: String   
    SERY_TEMKD?: String  
    TYSO_TI?: String    
    TYSO_TU?: String   
    KIM_CHITAI?: String    
    SO_CHITAI?: String   
    SO_BBAN_KD?: String    
    TTHAI_INTTHAO?: String  
    SO_PHA?: String
    DIEN_AP?: String
    VH_CONG?: String 
}

export interface ICongto {
    MA_CTO?: String
    SO_CTO?: String 
    NAM_SX?: String  
    MA_CLOAI?: String   
    SO_HUU?: Number  
    MA_DVI_SD?: String    
    SLAN_SDUNG?: Number   
    TTRANG_KD?: Number   
    MA_BDONG?: String   
    NGAY_BDONG?: String  
    SO_BBAN?: String    
    NGAY_KDINH?: String   
    SLAN_IN?: Number    
    MTEM_KD?: Number   
    SERY_TEMKD?: String  
    TYSO_TI?: Number    
    TYSO_TU?: Number   
    KIM_CHITAI?: String    
    SO_CHITAI?: Number   
    SO_BBAN_KD?: String    
    TTHAI_INTTHAO?: Number  
}


export class HsoTu {
    MA_TU?: String
    SO_TU?: String   
    NAM_SX?: String   
    MA_CLOAI?: String  
    SO_HUU?: String   
    MA_DVI_SD?: String   
    SLAN_SDUNG?: String   
    TTRANG_KD?: String   
    MA_BDONG?: String   
    NGAY_KDINH?: String  
    SO_BBAN_KD?: String   
    MA_DVIKD?: String   
    MA_NVIENKD?: String  
    MCHI_KDINH?: String    
    SO_CHIKD?: String   
    TTHAI_INTTHAO?: String  
    TTRANG_CH?: String
    LOAI_TU?: String;
    SO_PHA?: String;
    DIEN_AP?: String;
    TYSO_DAU?: String;    
}

export class HsoTi {
    MA_TI?: String 
    SO_TI?: String    
    NAM_SX?: String   
    MA_CLOAI?: String   
    SO_HUU?: String   
    MA_DVI_SD?: String   
    SLAN_SDUNG?: String  
    TTRANG_KD?: String   
    MA_BDONG?: String   
    NGAY_KDINH?: String   
    SO_BBAN_KD?: String   
    MA_DVIKD?: String  
    MA_NVIENKD?: String  
    MCHI_KDINH?: String   
    SO_CHIKD?: String  
    TTHAI_INTTHAO?: String    
    TTRANG_CH?: String  
    LOAI_TI?: String;
    SO_PHA?: String;
    DIEN_AP?: String;
    TYSO_DAU?: String; 
}

export class Vattu {
    MA_VTU: String
    MA_DDO_DDIEN: String   
    MA_DVIQLY: String    
    TEN_VTU: String    
    MA_LOAI_CPHI: String   
    SO_LUONG: String    
    DON_GIA: String   
    SO_HUU: String   
    DVI_TINH: String   
    STT: String    
    DON_GIA_NC_LAP: String    
    DON_GIA_NC_DIDOI: String    
    BAC_THO: String  
    NGAY_HLUC: String  
}