export class BoPhan {
    MA_BPHAN?: String
    TEN_BPHAN?: String
}


export interface IBoPhan {
    MA_BPHAN?: String
    TEN_BPHAN?: String
}

export class NVBoPhan {
    MA_NVIEN: String
    TEN_NVIEN?: String
}

export interface INVBoPhan {
    MA_NVIEN: String
    TEN_NVIEN?: String
}