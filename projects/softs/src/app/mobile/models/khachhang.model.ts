export class Khachhang {
    id?: string;
    code?: string;
    name?: string;
    description?: string;
    price?: number;
    quantity?: number;
    inventoryStatus?: string;
    category?: string;
    image?: string;
    rating?: number;
}

export class Customer {
    MA_DVIQLY?: string;
    ID_KHANG?: number;
    MA_KHANG?: string;
    TEN_KHANG?: string;
    DIA_CHI?: string;
    NGAY_SINH?: any;
    DTHOAI?: string;
    DTHOAI_DVU?: string;
    EMAIL?: string;
    GIOI_TINH?: number;
    LOAI_KHANG?: number;
    MA_LOAIHD?: string;
    MANHOM_KHANG?: string;
    MA_NN?: string;
    MA_SOGCS?: string;
    MA_TRAM?: string;
    MA_KVUC?: string;
    STT?: string;
    TRANG_THAI?: number;
} 
