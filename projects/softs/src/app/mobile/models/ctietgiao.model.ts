import { Customer } from './khachhang.model'
export class Ctietgiao {
    SO_BIEN_BAN?: number;
    MA_DVIQLY?: string;
    MA_QLKH?: number;
    MA_NVKS?: number;
    NGAY_GIAO?: any;
    TRANG_THAI?: number;
    LST_KHANG: Customer[];
}
