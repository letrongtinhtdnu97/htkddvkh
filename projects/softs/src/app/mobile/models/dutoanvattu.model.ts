export class DTVTKhachHang {
    ID_VATTU_CTIET?: number
    MA_DDO_DDIEN?: number   
    MA_VTU?: String    
    TEN_VTU?: String    
    MA_LOAI_CPHI?: String    
    SO_LUONG?: number    
    DON_GIA?: String    
    SO_HUU?: String   
    DVI_TINH?: String    
    TINH_TIEN?: String   
    STT_BC?: number   
    MA_YCAU_KNAI?: String 
    LOAI_VTU?: String   
}