import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';

@Component({
  selector: 'tao-bien-ban',
  templateUrl: './tao-bien-ban.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
      :host ::ng-deep button {
        margin-right: .25em;
        margin-left: .25em;
      }

      :host ::ng-deep .p-splitbutton button {
        margin-right: 0;
        margin-left: 0;
      }`],
  providers: [MessageService, ConfirmationService, DatePipe]
})
export class TaoBienBanComponent implements OnInit {

  bienbanDialog: boolean;

  bienbans: any[];

  bienban: any;
  products: any[]
  selectedBienban: any;

  khachhangs: any[];

  khachhang: any;

  selectedKhangs: any[];
  selectedProducts: any;
  submitted: boolean;

  cols: any[];

  items: MenuItem[];

  login: any;
  dataEmployee: any;
  constructor(

    private messageService: MessageService,

    private confirmationService: ConfirmationService, 
    private router: Router,
    private breadcrumbService: BreadcrumbService, 
    private datePipe: DatePipe) {
    this.breadcrumbService.setItems([
      { label: 'Điều tra TTKH' },
      { label: 'Nhận biên bản' }
    ]);
  }

  ngOnInit(): void {
    this.login = JSON.parse(localStorage.getItem('user'));
    this.loadHR();
    this.products = new Array();
  }
  loadHR():void {
    const param = {
      ORG_CODE: this.login.ORG_CODE,
      EMPLOYEE_ID: this.login.EMPLOYEE_ID
    }
   
    
  }
  loadBBanChotTHien(): void {
   
  }

  getTThaiBienBan(trangthai: number): string {
    switch (trangthai) {
      case 0: {
        return 'Mới giao';
      }
      case 1: {
        return 'Nhận mobile';
      }
      case 2: {
        return 'Đồng bộ';
      }
      case 3: {
        return 'Đã chốt';
      }
      case 4: {
        return 'Nhận CMIS';
      }
    }
  }

  onRowSelect(event): void {
    try {
      const param = {
        MA_DVIQLY: event.data.MA_DVIQLY,
        SO_BIEN_BAN: event.data.SO_BIEN_BAN
      };

     
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi tải dữ liệu, vui lòng thử lại sau.' });
    }
  }

  back(): void {
    this.router.navigateByUrl('/qtoan/list');
  }

  save(): void {
    try {
      const param = {
        MA_DVIQLY: this.selectedBienban.MA_DVIQLY,
        SO_BIEN_BAN: this.selectedBienban.SO_BIEN_BAN,
        NGAY_NHAN: this.datePipe.transform(Date(), 'dd/MM/yyyy'),
        LST_KHANG: this.selectedKhangs,
        NGUOI_THIEN: this.login.USER_NAME
      };
      
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Nhận biên bản không thành công' });
      console.log('saveBienBanNhanVaoCMIS Error is: ', error);
    }
  }

}
