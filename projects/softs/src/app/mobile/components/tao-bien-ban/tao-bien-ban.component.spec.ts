import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaoBienBanComponent } from './tao-bien-ban.component';

describe('TaoBienBanComponent', () => {
  let component: TaoBienBanComponent;
  let fixture: ComponentFixture<TaoBienBanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaoBienBanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaoBienBanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
