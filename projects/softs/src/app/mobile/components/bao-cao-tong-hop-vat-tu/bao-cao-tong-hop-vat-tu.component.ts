import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { NVBoPhan } from '../../models/bophan.model';
import { RP_VATTU } from '../../models/report.model';
import { BaoCaoService } from '../../services/bao-cao.service';
import { CmisService } from '../../services/cmis.service';



@Component({
  selector: 'app-bao-cao-tong-hop-vat-tu',
  templateUrl: './bao-cao-tong-hop-vat-tu.component.html',
  styleUrls: ['./bao-cao-tong-hop-vat-tu.component.css'],
  providers: [DatePipe]
})
export class BaoCaoTongHopVatTuComponent implements OnInit {

    vattus: RP_VATTU[];
    
    vattu: RP_VATTU;
    maDvily: String;
    selectedVattu: RP_VATTU[];
    nhanviens: any;
    selectedNhanvien: string = '-1';
    isLoading: boolean = false;
    tuNgay: any;
    denNgay: any;
    rowGroupMetadata: any;

    constructor(
      private reportService: BaoCaoService,
      private cmisService: CmisService,
      private datePipe: DatePipe
    ) { }

    ngOnInit() {
        this.maDvily = localStorage.getItem('MADVI_KD').toString();
        

        this.loadInitData();
        this.loadNhanVienData();

    }
    loadNhanVienData():void {
      const body = {
        MA_DVI: this.maDvily,
        MA_BPHAN: "ALL"
    }
    
    this.isLoading = true
      this.cmisService.dSNhanVienToBoPhan(body)
        .subscribe(
          (data: any) => {
            if(data.TYPE || data.MESSAGE) {
              this.nhanviens = []
            } else {
              this.nhanviens = [{value: '-1' , label: 'Chọn nhân viên'}]
              data.forEach((item: NVBoPhan) => {
                this.nhanviens.push({value: item.MA_NVIEN, label: item.MA_NVIEN + ' - ' + item.TEN_NVIEN})
              })
            }
          },
          (err: any) => {
            this.nhanviens = []
          },
          () => {
            this.isLoading = false;
          }
        )
    }
    reportExecl():void {
      const data = this.rowGroupMetadata;
      console.log(data);
      this.reportService.showReportVattu(
        this.vattus,
        this.datePipe.transform(this.tuNgay,'dd/mm/yy'),
        this.datePipe.transform(this.denNgay,'dd/mm/yy'),data)
    }
    reportExecl1():void {
      const data = this.rowGroupMetadata;
      console.log(data);
      this.reportService.showReportCtoPhanBo(
        this.vattus,
        this.datePipe.transform(this.tuNgay,'dd/mm/yy'),
        this.datePipe.transform(this.denNgay,'dd/mm/yy'),data)
    }
    reportExecl2():void {
      const data = this.rowGroupMetadata;
      console.log(data);
      this.reportService.showReportKhDThicong(
        this.vattus,
        this.datePipe.transform(this.tuNgay,'dd/mm/yy'),
        this.datePipe.transform(this.denNgay,'dd/mm/yy'),data)
    }
    reportExecl3():void {
      const data = this.rowGroupMetadata;
      console.log(data);
      this.reportService.showReportQtoanCto(
        this.vattus,
        this.datePipe.transform(this.tuNgay,'dd/mm/yy'),
        this.datePipe.transform(this.denNgay,'dd/mm/yy'),data)
    }
    
    loadInitData():void {
      const body = {
        MA_DVIQLY : this.maDvily,
        MA_NVIEN_TCONG : "",
      }
      
      this.isLoading = true
      this.reportService.reportVatTu(body)
        .subscribe(
          (data: any) => {
            if(data?.TYPE || data?.MESSAGE ) {
              this.vattus = [];
            } else {
              this.vattus = data;
            }
          },
          (err: any) => {
            this.vattus = [];
          },
          () => {
            this.isLoading = false;
          }
        )
    }
    onSort() {
        this.updateRowGroupMetaData();
    }

    updateRowGroupMetaData() {
        this.rowGroupMetadata = {};

        if (this.vattus) {
            for (let i = 0; i < this.vattus.length; i++) {
                let rowData = this.vattus[i];
                let representativeName = rowData.TEN_NVIEN_TCONG;
                
                if (i == 0) {
                    this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
                }
                else {
                    let previousRowData = this.vattus[i - 1];
                    let previousRowGroup = previousRowData.TEN_NVIEN_TCONG;
                    if (representativeName === previousRowGroup)
                        this.rowGroupMetadata[representativeName].size++;
                    else
                        this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
                }
            }
        }
    }

}
