import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaoCaoTongHopVatTuComponent } from './bao-cao-tong-hop-vat-tu.component';

describe('BaoCaoTongHopVatTuComponent', () => {
  let component: BaoCaoTongHopVatTuComponent;
  let fixture: ComponentFixture<BaoCaoTongHopVatTuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaoCaoTongHopVatTuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaoCaoTongHopVatTuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
