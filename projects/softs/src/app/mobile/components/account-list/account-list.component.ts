import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AccountService } from '../../services/account.service';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Profile, CategoryDevice, Account } from '../../models/account.model';

import { Employee,USER } from '../../models/account.model';
import { DeviceService } from '../../services/device.service';
import { Device } from '../../models/device.model';
export interface City {
  name: string,
  code: string,
  inactive: boolean
}
@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./account-list.component.css'],
  providers: [MessageService, ConfirmationService]
})

export class AccountListComponent implements OnInit {

  categories:  any[]
  accountDialog: boolean;
  selectCategory: any;
  accounts: Account[];
  status: number = 1;
  account: Account;

  selectedAccounts: Account[];

  submitted: boolean;

  cols: any[];

  items: MenuItem[];
  login: USER;
  dataEmployee: Employee;
  maDv_ql: any;
  constructor(
    private accountService: AccountService, 
    private deviceService: DeviceService,
    private messageService: MessageService,

    private confirmationService: ConfirmationService, 
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Tài khoản trên điện thoại' },
      { label: 'Tài khoản' }
    ]);
  }
  
  ngOnInit(): void {
    this.login = JSON.parse(localStorage.getItem('user'));
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.loadData();
    this.cols = [
      { field: 'userAccount', header: 'Tài khoản' },
      { field: 'phone', header: 'Số điện thoại' },
      { field: 'nameDevice', header: 'Tên thiết bị' },
      { field: 'status', header: 'Trạng thái' }
    ];

    this.items = [
      { label: 'Edit access device', icon: 'pi pi-mobile', command: (event: Event) => { this.openNew(); } }
    ];
    
  }
  
  loadCategories(): void {
    const param = {
      ORG_CODE: this.maDv_ql
    }
    this.deviceService.GetDeviceNotConnectAccount(param)
      .subscribe(
        (data: any) => {
          if(data?.TYPE) {

          } else {
            if(data?.length > 0) {
              this.categories = [];
              data.map((item: Device) => {
                if(item.ACTIVE === 1) {
                  this.categories.push({label: item.DEVICE_NAME + " - "+ item.IMEI, value: item.DEVICE_ID, inactive: item.ACTIVE === 1 ? false : true })
                }
              })
            }
          }
        }
      )
    
    
  }
  loadData(): void {
    const param = {
      ORG_CODE: this.maDv_ql
    }
    this.accountService.GetAll(param)
      .subscribe(
        (data: any) => {
          console.log(data)
          if(data.TYPE) {

          } else {
            this.accounts = data;
          }
        },
        (err: any) => {
          console.log(err);
        },
        () => {
          this.loadCategories();
        }
      )
    
    
  }
  openNew(): void {
    this.submitted = false;
    this.accountDialog = true;
    this.account = new Account();
  }

  deleteSelectedAccounts(): void {
    this.confirmationService.confirm({
      message: 'Bạn có muốn xóa các tài khoản?',
      header: 'Thông báo',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          listArr: this.selectedAccounts
        };
        
      }
    });
  }

  editAccount(account: Account): void {
    this.account = { ...account };
    this.accountDialog = true;
    
  }

  deleteAccount(account: Account): void {
    this.confirmationService.confirm({
      message: 'Bạn có muốn xóa tài khoản ' + account.ACCOUNT_NAME + '?',
      header: 'Thông báo',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          userid: account.ACCOUNT_ID
        }
       
      }
    });
  }
  validateEmail(email): boolean {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  hideDialog(): void {
    this.accountDialog = false;
    this.submitted = false;
    
  }
  getHR():void {

  }
  telephoneCheck(str): boolean {
    const ex = /(03|05|07|08|09|01[2|6|8|9]|02|03|04|05|06|07|08|09)+([0-9]{9}|[0-9]{8})\b/
      if(!str) {
          return false
      }
      return ex.test(str)
  }
  checkPassword(str): boolean {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(str);
  }
  saveAccount(): void {
    this.submitted = true;
    if(this.account.ACCOUNT_ID) {
      let active = this.status ? 1 : 0
      let param: Account = new Account();
      if(this.selectCategory) {
        param = {
          ...this.account,
          STATUS: active,
          DEVICE_ID: this.selectCategory,
          CREATED_BY: this.login.USER_NAME,
          ORG_CODE: this.maDv_ql
        }
      } else {
        param = {
          ...this.account,
          STATUS: active,
          CREATED_BY: this.login.USER_NAME,
          ORG_CODE: this.maDv_ql
        }
      }
     
      this.accountService.UpdateAccount(param)
        .subscribe(
          (data: any) => {
            if(data.MESSAGE === 'SUCCESS' && data.TYPE === 'SUCCESS') {
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Chỉnh sửa tài khoản thành công.' });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Chỉnh sửa không tài khoản thành công.' });
            }
          },
          (err: any) => {
            this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Chỉnh sửa không tài khoản thành công.' });
          },
          () => {
            this.loadData();
          }
        )
    } else {
      let active = this.status ? 1 : 0
      let param: Account = new Account();
      if(this.selectCategory) {
        param = {
          ...this.account,
          STATUS: active,
          DEVICE_ID: this.selectCategory,
          CREATED_BY: this.login.USER_NAME,
          PASSWORD_ALGORITHM: 'rsa',
          EMPLOYEE_ID: 1,
          ORG_CODE: this.maDv_ql
        }
      } else {
        param = {
          ...this.account,
          STATUS: active,
          EMPLOYEE_ID: 1,
          PASSWORD_ALGORITHM: 'rsa',
          CREATED_BY: this.login.USER_NAME,
          ORG_CODE: this.maDv_ql
        }
      }
      
      this.accountService.AddAccount(param)
        .subscribe(
          (data: any) => {
            if(data.MESSAGE === 'SUCCESS' && data.TYPE === 'SUCCESS') {
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Tạo mới tài khoản thành công.' });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Tạo mới không tài khoản thành công.' });
            }
          },
          (err: any) => {
            this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Tạo mới không tài khoản thành công.' });
          },
          () => {
            this.loadData();
          }
        )
    }
     
    
  }
  
}
