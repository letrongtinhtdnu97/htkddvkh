import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisTuComponent } from './bb-cmis-tu.component';

describe('BbCmisTuComponent', () => {
  let component: BbCmisTuComponent;
  let fixture: ComponentFixture<BbCmisTuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisTuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisTuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
