import { Component, OnInit,Output ,EventEmitter} from '@angular/core';
import { HsoTu } from '../../../models/congto.model';
import { CmisService } from '../../../services/cmis.service';

@Component({
  selector: 'app-bb-cmis-tu',
  templateUrl: './bb-cmis-tu.component.html',
  styleUrls: ['./bb-cmis-tu.component.css']
})
export class BbCmisTuComponent implements OnInit {
  @Output() onListData = new EventEmitter<any>();
  display: boolean = false;
  vattus: any[] = [];
  selectedVattu: any[];
  count: any;

  hsoTus: HsoTu[];
  selectedHsoTu: HsoTu[];
  hosTu: HsoTu;

  listHosoTu: HsoTu[]
  maDv_ql: String;
  constructor(
    private cmisService: CmisService
  ) { }

  ngOnInit(): void {
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.loadData();
  }
  save() {
    this.listHosoTu = this.selectedHsoTu ;
    console.log(this.listHosoTu);
    this.display = false;
    this.onListData.emit(this.listHosoTu);

  }
  hideDialog() {
    this.display =false;
  }
  loadData():void {
    const param = {
      MA_DVI: this.maDv_ql
    }
    this.cmisService.dsHsoTu(param)
      .subscribe(
        (data: any) => {
          if(data.TYPE || data.MESSAGE) {
            return
          } else {
            this.hsoTus = data
          }
        },
        (err: any) => {
          this.hsoTus = [];
        }
      )
  }
  showDialog() {
      this.display = true;
  }
}
