import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisAddComponent } from './bb-cmis-add.component';

describe('BbCmisAddComponent', () => {
  let component: BbCmisAddComponent;
  let fixture: ComponentFixture<BbCmisAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
