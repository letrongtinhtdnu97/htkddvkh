import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisVattuComponent } from './bb-cmis-vattu.component';

describe('BbCmisVattuComponent', () => {
  let component: BbCmisVattuComponent;
  let fixture: ComponentFixture<BbCmisVattuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisVattuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisVattuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
