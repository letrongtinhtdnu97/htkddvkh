import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Vattu } from '../../../models/congto.model';
import { DTVTKhachHang } from '../../../models/dutoanvattu.model';
import { CmisService } from '../../../services/cmis.service';

@Component({
  selector: 'app-bb-cmis-vattu',
  templateUrl: './bb-cmis-vattu.component.html',
  styleUrls: ['./bb-cmis-vattu.component.css']
})
export class BbCmisVattuComponent implements OnInit {
  @Output() onListData = new EventEmitter<any>();
  display: boolean = false;
  vattus: DTVTKhachHang[] = [];
  selectedVattu: DTVTKhachHang[];
  vattu: DTVTKhachHang;

  listVatTu: DTVTKhachHang[];
  maDv_ql: String;
  constructor(
    private cmisService: CmisService
  ) { }

  ngOnInit(): void {
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.loadData();
    
  }
  save() {
    this.listVatTu = this.selectedVattu.map(item => {
      return {
        ...item,
        TINH_TIEN: item.TINH_TIEN ? "1" : "0"
      }
    });
    console.log('this.listVatTu',this.listVatTu)
    this.display = false;
    this.onListData.emit(this.listVatTu);


  }
  hideDialog():void {
    this.display = false;
  }
  loadData():void {
    const param = {
      MA_DVI: this.maDv_ql
    }
    this.cmisService.dsVatTu(param)
      .subscribe(
        (data: any) => {
          if(data.TYPE || data.MESSAGE) { 
            return
          } else {
            this.vattus = data;
          }
        },
        (err: any) => {
          this.vattus = []
        }
      )
  }
  showDialog() {
      this.display = true;
  }
}
