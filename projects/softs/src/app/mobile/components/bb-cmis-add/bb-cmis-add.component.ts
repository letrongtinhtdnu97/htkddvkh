
import { Component, OnInit, Input,ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Khachhang } from '../../models/Khachhang.model';
import { Bbandtttkh } from '../../models/bbandtttkh.model';
import { BienbanService } from '../../services/bienban.service';
import { AccountService } from '../../services/account.service';

import { Employee,USER } from '../../models/account.model';
import { BoPhan, NVBoPhan } from '../../models/bophan.model';
import { CmisService } from '../../services/cmis.service';

import { YCThiCong } from '../../models/yeucauthicong.model';
import { CongTo, HsoTi, HsoTu, Vattu } from '../../models/congto.model';
import { DTVTKhachHang } from '../../models/dutoanvattu.model';
import { KHKhaoSat } from '../../models/khkhaosat.model';
import { combineLatest, forkJoin } from 'rxjs';
import { retry } from 'rxjs/operators';


@Component({
  selector: 'app-bb-cmis-add',
  templateUrl: './bb-cmis-add.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  providers: [MessageService, ConfirmationService, DatePipe]
})


export class BbCmisAddComponent implements OnInit {
  @ViewChild('vattu') componentVattu;
  @ViewChild('tu') componentTu;
  @ViewChild('ti') componentTi;
  @ViewChild('thietbi') componentThietbi;

  listHinhHsoCmis: any;
  listHinhHso: any[];
  listBoPhan: any[];
  selectedBoPhan: String;
  dsYeuCauThiCong: YCThiCong[];
  yCauThiCong: YCThiCong;
  selectedYeucauthicong: YCThiCong[] = [];
  congtos: CongTo[];
  congto: CongTo;
  selectedCongto: CongTo[];
  vattus: Vattu[];
  vattusCurrent: Vattu[] = [];
  vattu: Vattu;
  selectedVattu: Vattu[];

  dtoanVattus: DTVTKhachHang[];
  selectedDtoanVattu: DTVTKhachHang[];
  dtoanVattu: DTVTKhachHang;
  dtoanVattuCurrent: any;
  ListKHKhaoSat: KHKhaoSat[] = [];
  hsoTus: HsoTu[]
  hsoTu: HsoTu;
  selectedHsoTu: HsoTu[];

  hsoTis: HsoTi[];
  hsoTi: HsoTi;
  selectedHsoTi: HsoTi[]

  bbdtttkh: Bbandtttkh;

  customers: Khachhang[];

  customer: Khachhang;

  selectedCustomers: Khachhang[] = [];

  submitted: boolean;

  selectedNguoiTH: any = null;

  nguoiths: any[];

  selectedSoGCSs: any[] = [];

  soGCSs: any[];

  selectedTrams: any[] = [];

  trams: any[];

  minDate: Date;
  ngayGiao: Date = new Date;
  dataKHs: any[];
  login: USER;
  dataEmployee: Employee;
  maDv_ql: any;
  timer: boolean = true;
  display: boolean = false;
  soBienBan: String;

  @Input() item = '';
  constructor(private bbanService: BienbanService, 
    private messageService: MessageService,
    private confirmationService: ConfirmationService, 
    private router: Router,
    private breadcrumbService: BreadcrumbService, 
    private datePipe: DatePipe,
    private cmisService: CmisService
              ) {
    this.breadcrumbService.setItems([
      { label: 'Điều tra TTKH' },
      { label: 'Thêm biên bản giao' }
    ]);
  }

  ngOnInit(): void {
    this.login = JSON.parse(localStorage.getItem('user'));
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.bbdtttkh = new Bbandtttkh();
    this.bbdtttkh.NGAY_GIAO = new Date();
    this.minDate = new Date();

    this.loadHR();
    this.loadBoPhan();
    
  }
  ngOnDestroy(): void {

  }
  public loadListVatTu(event):void {
    console.log(this.dtoanVattuCurrent)
    if(!this.dtoanVattuCurrent || this.dtoanVattuCurrent.length < 1) {
      this.dtoanVattus = event;
    } else {
      this.dtoanVattus = this.dtoanVattuCurrent.concat(event);
    }
    
    console.log('this.vattusCurrent',this.dtoanVattus)
  }
  public loadListTu(event):void {
    console.log(event);
    this.hsoTus = event;
  }
  public loadListTi(event):void {
    this.hsoTis = event;
  }
  public loadListThietbi(event):void {
    this.congtos = event;
  }


  //bo phan
  loadBoPhan():void {
    const param = {
      MA_DVI: this.maDv_ql
    }
    this.cmisService.dSBoPhan(param)
      .subscribe(
        (response: any) => {
          this.listBoPhan = [];
          console.log(response)
          if(response.MESSAGE || response.TYPE) {
            return;
          }
          response.forEach((element: BoPhan) => {
            this.listBoPhan.push({value: element.MA_BPHAN , label: element.MA_BPHAN.toString() +' - '+ element.TEN_BPHAN})
          });
          let unique = this.listBoPhan.filter((v, i, a) => a.indexOf(v) === i);
         
          this.listBoPhan = unique;
        }
      )
  }
  loadNgThien(event):void {
    this.selectedNguoiTH = null;
    if(!event?.value) {
      return
    }
    const param = {
      MA_DVI: this.maDv_ql,
      MA_BPHAN: event.value ? event.value : 'ALL'
    }
    this.cmisService.dSNhanVienToBoPhan(param)
      .subscribe(
        (response: any) => {
          this.nguoiths = []
          if(response.TYPE || response.MESSAGE) {
            return
          }
          response.forEach((element: NVBoPhan) => {
            this.nguoiths.push({name: element.TEN_NVIEN,label: element.MA_NVIEN.toString() + ' - ' + element.TEN_NVIEN, value: element.MA_NVIEN })
          });
        },
        (error: any) => {
          this.nguoiths = []
        }
      )

  }

  loadInitData(event):void {
    this.display = true;
    this.loadYCKhachHang();
    // this.loadCongto();
    // this.loadVatTu();
    // this.loadHsoTi();
    // this.loadHsoTu();
    // this.display = false;
  }

  loadHosokhangtoCmis(maDvi, maYc):void {
    const param = {
      AccessKey: "123",
      SecretKey: "123",
      strMa_Dviqly: maDvi,
      strMa_YCau: maYc,
      TYPE: "KH"
    }
    this.cmisService.dsHsoToCmis(param)
      .subscribe(
        (data: any) => {
          console.log(data);
        },
        (err: any) => {
          console.log(err)
        },
        () => {

        }
      )
  }

  loadYCKhachHangToKCSD(list):void {

    const param = {
      LIST_YC: list
    }
    this.cmisService.dsKHKhaoSat(param)
      .subscribe(
        (res: any) => {
            if(res.TYPE || res.MESSAGE) {
              this.ListKHKhaoSat = [];
              return;
            } else {
              this.ListKHKhaoSat = this.ListKHKhaoSat.concat(res)
            }
        },
        (err: any) => {
          this.ListKHKhaoSat = [];
        }
      )
  }
  loadYCKhachHang():void {
    const param = {
      MA_DVI: this.maDv_ql,
      MA_BPHAN_NHAN: this.selectedBoPhan,
      MA_NVIEN_NHAN: this.selectedNguoiTH
    }
    this.cmisService.dsYeuCauThiCong(param)
      .subscribe(
        (response: any) => {
          this.dsYeuCauThiCong = [];
          if(response.TYPE || response.MESSAGE) {
            return
          } else {
            this.dsYeuCauThiCong = response
          }
        },
        (error: any) => {
          
          this.dsYeuCauThiCong = [];
        },
        () => {
          this.display = false;
        }
        
      )
  }

  deleteVattu():void {
    let arr1 = []
    this.vattus.forEach((item: Vattu) => {
      let arr = this.selectedVattu.find((item1: Vattu) => item.MA_VTU ===  item1.MA_VTU)
      if(!arr) {
        arr1.push(item);
      }
    })
    this.selectedVattu = [];
    this.vattus = arr1;
    this.componentVattu.selectedVattu = arr1;
  }
  deleteHsoTu():void {

    let arr1 = [];
    this.hsoTus.forEach((item: HsoTu) => {
      let arr = this.selectedHsoTu.find((item1: HsoTu) => item.MA_TU ===  item1.MA_TU)
      if(!arr) {
        arr1.push(item);
      }
    })

    this.hsoTus = arr1;
    this.selectedHsoTu = [];
    this.componentTu.selectedHsoTu = arr1;
  }
  deleteCongto():void {
    let arr1 = [];
    this.congtos.forEach((item: CongTo) => {
      let arr = this.selectedCongto.find((item1: CongTo) => item.MA_CTO ===  item1.MA_CTO)
      if(!arr) {
        arr1.push(item);
      }
    })
    console.log(arr1);
    this.congtos = arr1;
    this.selectedCongto = [];
    this.componentThietbi.selectedCongto = arr1;
  }
  deleteHsoTi():void {
    let arr1 = [];
    this.hsoTis.forEach((item: HsoTi) => {
      let arr = this.selectedHsoTi.find((item1: HsoTi) => item.MA_TI ===  item1.MA_TI)
      if(!arr) {
        arr1.push(item);
      }
    })

    this.hsoTis = arr1;
    this.selectedHsoTi = [];
    this.componentTi.selectedHsoTi = arr1;
  }
  addVatTu():void {
    this.componentVattu.showDialog();
  }
  addTu():void {
    this.componentTu.showDialog();
  }
  addTi():void {
    this.componentTi.showDialog();
  }
  addThietbi():void {
    this.componentThietbi.showDialog();
  }

  loadDuToan():void {
    this.display = true;
    const param  = {
      LIST_MA: this.selectedYeucauthicong
    }
    const param2 = {
      LIST_YC: this.selectedYeucauthicong.map(item => {
        return {
          ...item,
          MA_DVI: item.MA_DVIQLY
        }
      })
    }
    
    const param4 = {
      LIST_YC: this.selectedYeucauthicong
    }
    combineLatest([
      this.cmisService.dsDuToanVatTuKhachHang(param),
      this.cmisService.dsKHKhaoSat(param2),
      this.cmisService.dsHsoToCmis(param4),
      this.cmisService.dsHinhKhaoSat(param4),
    ])
    .subscribe((resArray: any) => {
      if(resArray.length > 0) {
        if(
          resArray[0].TYPE || resArray[0].MESSAGE || 
          resArray[1].TYPE || resArray[1].MESSAGE || 
          resArray[2].TYPE  || resArray[2].MESSAGE   ||
          resArray[3].TYPE || resArray[3].MESSAGE 
          ) {
          return;
        } else {
          this.dtoanVattuCurrent  = resArray[0];
          this.ListKHKhaoSat = resArray[1]
          this.listHinhHsoCmis = resArray[2]
          this.listHinhHso = resArray[3]
        }
      }
    },(err) =>{
      this.dtoanVattuCurrent = []
      this.ListKHKhaoSat = []

    },()=> {
      this.dtoanVattus = this.dtoanVattuCurrent;
      this.display = false;
      
    })

     
    // this.cmisService.dsDuToanVatTuKhachHang(param)
    //   .subscribe(
    //     (data: any) => {
    //       if(data.TYPE || data.MESSAGE) {
    //         this.messageService.add({severity:'warn', summary: 'Thông báo', detail: 'Không có vật tư nào thuộc các yêu cầu đã chọn.', life: 3000});
    //         return;
    //       } else {
    //         this.vattusCurrent = data;
    //         this.dtoanVattuCurrent = data;
    //         console.log('this.dtoanVattuCurrent',this.dtoanVattuCurrent)
    //         return;
    //       }
    //     },
    //     (err: any) => {

    //     },
    //   () => {
    //     this.dtoanVattus = this.dtoanVattuCurrent;
    //     this.vattus = this.vattusCurrent;
    //     this.loadYCKhachHangToKCSD(this.selectedYeucauthicong);
    //     this.display = false;
    //   }
    //   )
  }
  
  loadHR(): void {
    const param = {
      ORG_CODE: this.login.ORG_CODE,
      EMPLOYEE_ID: this.login.EMPLOYEE_ID
    }
    
  }
  loadAccountMobile(): void {
    const param = {
      ORG_CODE: this.maDv_ql
    };
    this.nguoiths = [];
    
  }

  loadSoGCS(): void {
    const param = {
      MA_DVIQLY: this.dataEmployee.MADVI_KD
    };
    this.soGCSs = [];
    this.bbanService.getSoGCSByCondition(param).subscribe((response: any) => {
      if (response && response.length > 0) {
        response.forEach(element => {
          this.soGCSs.push({ label: element.MA_SOGCS + '- ' + element.TEN_SOGCS, value: element.MA_SOGCS });
        });
      }
    });
  }

  loadTram(): void {
    const param = {
      MA_DVIQLY: this.dataEmployee.MADVI_KD
    };
    this.trams = [];
    this.bbanService.getTramByCondition(param).subscribe((response: any) => {
      if (response && response.length > 0) {
        response.forEach(element => {
          this.trams.push({ label: element.MA_TRAM + '- ' + element.TEN_TRAM, value: element.MA_TRAM });
        });
      }
    });
  }

  back(): void {
    this.router.navigateByUrl('/task/list');
  }
  
  save(): void {
    let arCongToFormatDate = []
    if(this.selectedCongto) {
      arCongToFormatDate = this.selectedCongto.map(item => {
        return {
          ...item,
          NGAY_KDINH: this.datePipe.transform(item.NGAY_KDINH, 'dd/mm/yy')
        }
      })
    }
    let resultKHKsat = []
    
    if(this.ListKHKhaoSat && this.ListKHKhaoSat.length > 0) {
      if(this.selectedYeucauthicong && this.selectedYeucauthicong.length > 0) {
        resultKHKsat = this.selectedYeucauthicong.map(item => {
          let ar =  this.ListKHKhaoSat.find(item1 => item1.MA_YCAU_KNAI === item.MA_YCAU_KNAI)
          if(ar) {
            return {
              ...item,
              ...ar
            };
          } else {
            return item;
          }
        })
      }
    }
    
    let cmisHinh = this.listHinhHsoCmis.map((uitem) => {
      if(uitem.MA_LOAI_HSO === 'HDSH') {
        return {
          ...uitem,
          TEN_HSO: 'Hợp đồng sinh hoạt'
        }
      }
      if(uitem.MA_LOAI_HSO === 'BBAN_TTHAO') {
        return {
          ...uitem,
          TEN_HSO: 'Biên bản treo tháo'
        }
      }
      if(uitem.MA_LOAI_HSO === 'PHUONGAN_CDIEN') {
        return {
          ...uitem,
          TEN_HSO: 'Phương án cấp điện'
        }
      }
      return {
        ...uitem,
        TEN_HSO: ''
      }
      
    })
    const param = {
      MA_DVIQLY: this.maDv_ql,
      MA_NVIEN_PCONG: this.login.USER_NAME,
      TEN_NVIEN_PCONG: this.login.FULL_NAME,
      MA_NVIEN_TCONG: this.selectedNguoiTH,
      TEN_NVIEN_TCONG: this.nguoiths.find(item => item.value === this.selectedNguoiTH).name,
      NGAY_PCONG: this.datePipe.transform(this.ngayGiao, 'dd/mm/yy') ,
      TSO_YCAU: this.selectedYeucauthicong.length,
      TSO_VTTB: this.selectedDtoanVattu.length,
      TRANG_THAI: 0,
      NGUOI_TAO: this.login.USER_NAME,
      NGUOI_SUA: this.login.USER_NAME,
      LIST_CTIET_PCONG: resultKHKsat.length !== 0 ? resultKHKsat : this.selectedYeucauthicong,
      LIST_TI_PBO: this.selectedHsoTi,
      LIST_CTO_PBO: arCongToFormatDate,
      LIST_TU_PBO: this.selectedHsoTu,
      LIST_VTU_PBO: this.selectedDtoanVattu,
      LIST_HINH_KSCD: this.listHinhHso,
      LIST_HINH_CMIS: cmisHinh
    }
    console.log('param ++++',param)
    
    if(this.selectedYeucauthicong.length > 0) {
      this.cmisService.insertTienXuLy(param)
        .subscribe(
          (data: any) => {
            if(data.TYPE === "ERROR") {
              this.messageService.add({severity:'error', summary: 'Thông báo', detail: 'Mã khách hàng đã được giao ở biên bản khác.', life: 3000});
              return
            }
            if(data.TYPE === 'SUCCESS') {
              if(data.MESSAGE) {
                this.soBienBan = data.MESSAGE;
                this.messageService.add({severity:'success', summary: 'Thông báo', detail: 'Lưu biên bản thành công.', life: 3000});
              }
            }else {
              this.soBienBan = "";
              this.messageService.add({severity:'error', summary: 'Thông báo', detail: 'Lưu biên bản không thành công.', life: 3000});
            }
          },
          (err: any) => {
            console.log(err)
            this.messageService.add({severity:'error', summary: 'Thông báo', detail: 'Lưu biên bản không thành công.', life: 3000});
          }
        )
    }
  }

  
  

}

