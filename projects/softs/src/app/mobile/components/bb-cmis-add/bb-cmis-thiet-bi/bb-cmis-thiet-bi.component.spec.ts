import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisThietBiComponent } from './bb-cmis-thiet-bi.component';

describe('BbCmisThietBiComponent', () => {
  let component: BbCmisThietBiComponent;
  let fixture: ComponentFixture<BbCmisThietBiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisThietBiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisThietBiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
