import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CongTo } from '../../../models/congto.model';
import { CmisService } from '../../../services/cmis.service';

@Component({
  selector: 'app-bb-cmis-thiet-bi',
  templateUrl: './bb-cmis-thiet-bi.component.html',
  styleUrls: ['./bb-cmis-thiet-bi.component.css']
})
export class BbCmisThietBiComponent implements OnInit {
  @Output() onListData = new EventEmitter<any>();
  display: boolean = false;
  congtos: CongTo[] = [];
  selectedCongto: CongTo[];
  congto: CongTo;

  listCongto: CongTo[];
  
  maDv_ql: String;
  constructor(
    private cmisService: CmisService
  ) { }

  ngOnInit(): void {
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.loadData()
  }

  save():void {
    this.listCongto = this.selectedCongto;
    this.onListData.emit(this.listCongto);
    this.display = false
  }
  
  loadData():void {
    const param = {
      MA_DVI: this.maDv_ql
    }
    this.cmisService.dsCongTo(param)
      .subscribe(
        (data: any) => {
          if(data.TYPE || data.MESSAGE) {
            return
          } else {
            this.congtos = data
          }
        },
        (err: any) => {
          this.congtos = [];
        }
      )
  }
  showDialog() {
      this.display = true;
  }
  hideDialog() {
    this.display = false
  }
}
