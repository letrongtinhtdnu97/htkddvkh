import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HsoTi } from '../../../models/congto.model';
import { CmisService } from '../../../services/cmis.service';

@Component({
  selector: 'app-bb-cmis-ti',
  templateUrl: './bb-cmis-ti.component.html',
  styleUrls: ['./bb-cmis-ti.component.css']
})
export class BbCmisTiComponent implements OnInit {
  @Output() onListData = new EventEmitter<any>();
  display: boolean = false;
  vattus: any[] = [];
  selectedVattu: any[];
  count: any;

  dsTis: HsoTi[]
  selectedDsTi: HsoTi[]
  dsTi: HsoTi
  maDv_ql: String;
  listDsti: HsoTi[]
  constructor(
    private cmisService: CmisService
  ) { }

  ngOnInit(): void {
    this.maDv_ql = localStorage.getItem("MADVI_KD")
    this.loadData()
  }
  save() {
    this.listDsti = this.selectedDsTi ;
    this.display = false;
    this.onListData.emit(this.listDsti);

  }
  hideDialog() {
    this.display =false;
  }
  loadData():void {
    const param = {
      MA_DVI: this.maDv_ql
    }
    this.cmisService.dsHsoTi(param).subscribe(
      (data: any) => {
        console.log(data);
        if(data.TYPE || data.MESSAGE) {
          return
        }
        
        this.dsTis = data;
      },
      (error: any) => {
        console.log(error)
        this.dsTis = []
      }
    )
  }
  showDialog() {
      this.display = true;
  }
}
