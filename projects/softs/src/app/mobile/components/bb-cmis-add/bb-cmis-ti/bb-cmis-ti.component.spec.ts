import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisTiComponent } from './bb-cmis-ti.component';

describe('BbCmisTiComponent', () => {
  let component: BbCmisTiComponent;
  let fixture: ComponentFixture<BbCmisTiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisTiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisTiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
