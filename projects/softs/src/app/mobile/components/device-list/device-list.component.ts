import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Device, UserDevice } from '../../models/device.model';
import { DeviceService } from '../../services/device.service';

import { Employee,USER } from '../../models/account.model';
@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./device-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class DeviceListComponent implements OnInit {

  deviceDialog: boolean;
  deviceDialog1: boolean;
  devices: Device[];
  status: Number = 1;
  device: Device;

  selectedDevices: Device[];

  submitted: boolean;

  cols: any[];

  items: MenuItem[];
  login: USER;
  dataEmployee: Employee;
  maDv_ql: any;
  constructor(private deviceService: DeviceService, private messageService: MessageService,
              private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'User Mobile' },
      { label: 'Devices' }
    ]);
  }

  ngOnInit(): void {
    this.login = JSON.parse(localStorage.getItem('user'));
    this.maDv_ql = localStorage.getItem('MADVI_KD');
    this.loadData();
    this.cols = [
      { field: 'nameDevice', header: 'Name' },
      { field: 'maImei', header: 'IMEI' },
      { field: 'serialNumber', header: 'SERIAL' },
      { field: 'userAccount', header: 'ACCOUNT' },
      { field: 'status', header: 'Status' }
    ];

    this.items = [
      { label: 'Edit access device', icon: 'pi pi-mobile', command: (event: Event) => { this.openNew(); } }
    ];
  }
  loadData(): void {
    const param = {
      ORG_CODE: this.maDv_ql
    }
    this.deviceService.GetDevice(param)
      .subscribe(
        (data: any) => {
          if(data.TYPE && data.MESSAGE) {

          } else {
            this.devices = data;
          }

        },
        (err:any) => {
          console.log(err);
        }
      )
    
  }
  openNew(): void {
    this.deviceDialog = true;
    this.device = new Device();
    //this.device = {};
    
    this.submitted = false;
    // this.deviceDialog = true;
    this.status = this.device.ACTIVE ? this.device.ACTIVE : 1;
  }

  deleteSelectedDevice(): void {
    this.confirmationService.confirm({
      message: 'Bạn có muốn xóa các thiết bị này?',
      header: 'Thông báo',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          listArr: this.selectedDevices
        };
        // this.deviceService.deleteDevices(JSON.stringify(body)).subscribe(
        //   (data: any) => {
        //     if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
        //       this.devices = this.devices.filter(val => !this.selectedDevices.includes(val));
        //       this.selectedDevices = null;
        //       this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Xóa thiết bị thành công', life: 3000 });
        //     }else {
        //       if(data.MESSAGE === 'DEVICE WORKING') {
        //         this.messageService.add({severity: 'warn', summary: 'Thông báo', detail: 'Có thiết bị đang được sử dụng!', life: 3000});
        //         return;
        //       }
        //       this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Thiết bị này hiện tại không thể xóa!', life: 3000});
        //       return;

        //     }
        //   },
        //   (err: any) => {
        //     this.messageService.add({
        //       severity: 'error',
        //       summary: 'Thông báo',
        //       detail: 'Thiết bị này hiện tại không thể xóa!',
        //       life: 3000
        //     });
        //   }
        // )
      }
    });
  }

  editDevice(device: Device): void {
    this.device = { ...device };
    this.status = this.device.ACTIVE ? this.device.ACTIVE : 1;
    this.deviceDialog = true;
  }

  deleteDevice(device: Device): void {
    this.confirmationService.confirm({
      message: 'Bạn có muốn xóa thiết bị ' + device.DEVICE_NAME + '?',
      header: 'Thông báo',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const param = {
          LST_IDS: [device]
        }
        this.deviceService.DeleteDevice(param)
          .subscribe(
            (data: any) => {
              console.log(data)
              if(data.TYPE === 'SUCCESS' && data.MESSAGE === 'SUCCESS') {
                this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Xóa thiết bị thành công', life: 3000 });
              } else {
                this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Thiết bị này hiện tại không thể xóa!', life: 3000});
              }
            },
            (err: any) => {
              console.log(err)
              this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Thiết bị này hiện tại không thể xóa!', life: 3000});
            },
            () => {
              this.loadData()
            } 
          )
      }
    });
  }

  hideDialog(): void {
    this.deviceDialog = false;
    this.submitted = false;
  }

  saveDevice(): void {
    this.submitted = true;
    
    if(this.device.DEVICE_ID) {
      let active = this.status ? 1 : 0 
      const param: Device = {
        ...this.device,
        CREATED_BY: this.login.USER_NAME,
        ORG_CODE: this.maDv_ql,
        ACTIVE: active
      };
      this.deviceService.UpdateDevice(param) 
        .subscribe(
          (data: any) => {
            if(data.MESSAGE === "SUCCESS") {
              this.deviceDialog = false;
              this.messageService.add({severity: 'success', summary: 'Thông báo', detail: 'Chỉnh sửa thành công thiết bị mới!', life: 3000});
            } else {
              this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Chỉnh sửa không thành công thiết bị mới!', life: 3000});
            }
          },
          (err: any) => {
            this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Chỉnh sửa không thành công thiết bị mới!', life: 3000});
          },
          () => {
            this.loadData();
          }
        )
    } else {
      let active = this.status ? 1 : 0 

      const param: Device = {
        ...this.device,
        CREATED_BY: this.login.USER_NAME,
        ORG_CODE: this.maDv_ql,
        ACTIVE: active
      };

      this.deviceService.AddDevice(param) 
        .subscribe(
          (data: any) => {
            if(data.MESSAGE === "SUCCESS") {
              this.deviceDialog = false;
              this.messageService.add({severity: 'success', summary: 'Thông báo', detail: 'Tạo thành công thiết bị mới!', life: 3000});
            } else {
              this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Tạo không thành công thiết bị mới!', life: 3000});
            }
          },
          (err: any) =>{
            this.messageService.add({severity: 'error', summary: 'Thông báo', detail: 'Tạo không thành công thiết bị mới!', life: 3000});
          },
          () => {
            this.loadData();
          }
        )
      
    }
     
    
  }

}
