import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Product } from '../../../demo/domain/product';
import { ProductService } from '../../../demo/service/productservice';
import { CmisService } from '../../services/cmis.service';
import { BienBanList } from './bienbanlist.model';

@Component({
  selector: 'app-bb-cmis-list',
  templateUrl: './bb-cmis-list.component.html',
  styleUrls: ['../../../demo/view/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService]
})
export class BbCmisListComponent implements OnInit {
  productDialog: boolean;
    
  products: Product[];

  product: Product;

  selectedProducts: Product[];

  submitted: boolean;
    bienbanlists: BienBanList[]
    bienbanlistSelected: BienBanList[]
    bienbanlist: BienBanList
  statuses: any[];
    maDvi: any;
  constructor(
    private productService: ProductService, 
    private messageService: MessageService, 
    private confirmationService: ConfirmationService,
    private _router: Router,
    private _route: ActivatedRoute,
    private cmisService : CmisService
  ) { }

  ngOnInit() {
    this.productService.getProducts().then(data => this.products = data);

   this.maDvi = localStorage.getItem("MADVI_KD");
   this.loadData();
}


deleteSelectedProducts() {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete the selected products?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.products = this.products.filter(val => !this.selectedProducts.includes(val));
            this.selectedProducts = null;
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
        }
    });
}

editProduct(product: Product) {
  this._router.navigate(['/task/edit/'+product.id]);
}
openNew() {
    this._router.navigate(['/task/add']);
}
loadData() {
    const param = {
        MA_DVIQLY: this.maDvi
    }
    this.cmisService.getAllBBanPhanCongToDvi(param)
        .subscribe(
            (response: any) => {
                if(response.TYPE || response.MESSAGE) {
                    this.bienbanlists =[];
                    this.messageService.add({severity:'warn', summary: 'Thong bao', detail: 'Khong co du lieu', life: 3000});
                } else {
                    this.bienbanlists = response
                }
            },
            (error: any) => {
                this.bienbanlists =[];
                this.messageService.add({severity:'error', summary: 'Thong bao', detail: 'Looix', life: 3000});
            },
            () => {
                console.log(this.bienbanlists)
            }
            
        )
}
loadColorNameStatus
loadNameStatus(status: any):String {
    switch (status) {
        case 0:
            return 'Mối giao';
        case 1:
            return 'Nhận mobile'
        case 2:
            return 'Đang thi công'
        case 3:
            return 'Đã thi công'
        case 4: 
            return 'Đã quyết toán'
        default:
            return ''
    }
}
deleteProduct(product: Product) {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete ' + product.name + '?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.products = this.products.filter(val => val.id !== product.id);
            this.product = {};
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
        }
    });
}

hideDialog() {
    this.productDialog = false;
    this.submitted = false;
}

saveProduct() {
    this.submitted = true;

    if (this.product.name.trim()) {
        if (this.product.id) {
            this.products[this.findIndexById(this.product.id)] = this.product;
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
        }
        else {
            this.product.id = this.createId();
            this.product.image = 'product-placeholder.svg';
            this.products.push(this.product);
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
        }

        this.products = [...this.products];
        this.productDialog = false;
        this.product = {};
    }
}

findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].id === id) {
            index = i;
            break;
        }
    }

    return index;
}

createId(): string {
    let id = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for ( var i = 0; i < 5; i++ ) {
        id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
}

}
