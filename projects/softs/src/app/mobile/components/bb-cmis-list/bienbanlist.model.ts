export class BienBanList {
    maDviqly?: string
    maNvienPcong?: string
    maNvienTcong?: string
    ngayPcong?: string
    ngaySua?: string
    ngayTao?: string
    nguoiSua?: string
    nguoiTao?: string
    soBbanTcong?: number
    tenNvienPcong?: string
    tenNvienTcong?: string
    trangThai?: number
    tsoVttb?: number
    tsoYcau?: number
}