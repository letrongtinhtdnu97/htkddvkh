import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisListComponent } from './bb-cmis-list.component';

describe('BbCmisListComponent', () => {
  let component: BbCmisListComponent;
  let fixture: ComponentFixture<BbCmisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
