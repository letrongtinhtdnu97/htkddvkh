import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuyettoanVattuComponent } from './quyettoan-vattu.component';

describe('QuyettoanVattuComponent', () => {
  let component: QuyettoanVattuComponent;
  let fixture: ComponentFixture<QuyettoanVattuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuyettoanVattuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuyettoanVattuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
