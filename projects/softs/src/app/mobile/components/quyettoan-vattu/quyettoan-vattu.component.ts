import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Product } from '../../../demo/domain/product';

@Component({
  selector: 'app-quyettoan-vattu',
  templateUrl: './quyettoan-vattu.component.html',
  styleUrls: ['../../../demo/view/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService]
})
export class QuyettoanVattuComponent implements OnInit {

  productDialog: boolean;
    
  products: Product[];

  product: Product;

  selectedProducts: Product[];

  submitted: boolean;
    bienbanlists: any[]
    bienbanlistSelected: any[]
    bienbanlist: any
  statuses: any[];
    maDvi: any;
  constructor(

    private messageService: MessageService, 
    private confirmationService: ConfirmationService,
    private _router: Router,
    private _route: ActivatedRoute,
  
  ) { }

  ngOnInit() {
    
   this.bienbanlists = new Array();
}


deleteSelectedProducts() {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete the selected products?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.products = this.products.filter(val => !this.selectedProducts.includes(val));
            this.selectedProducts = null;
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
        }
    });
}

openNew() {
  this._router.navigate(['/qtoan/create']);
}



}
