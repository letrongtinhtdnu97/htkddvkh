import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbCmisEditComponent } from './bb-cmis-edit.component';

describe('BbCmisEditComponent', () => {
  let component: BbCmisEditComponent;
  let fixture: ComponentFixture<BbCmisEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbCmisEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbCmisEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
