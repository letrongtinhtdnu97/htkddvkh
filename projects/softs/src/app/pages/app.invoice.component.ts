import {Component} from '@angular/core';
import {BreadcrumbService} from '../core/services/breadcrumb.service';

@Component({
    templateUrl: './app.invoice.component.html'
})
export class AppInvoiceComponent {

    constructor(private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: 'Pages'},
            {label: 'Invoice'}
        ]);
    }

    print(): void {
        window.print();
    }
}
