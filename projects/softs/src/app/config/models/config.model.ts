export class Config {
    env: {
        name: string
    };

    url: {
        qtht: string,
        hr: string,
        dtttkh: string,
        kddvkd: string,
        ktdlcmis: string,
        udht: String,
        hoso_cmis: String,
    };
}
