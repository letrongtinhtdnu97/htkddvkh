import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { LoginService } from '../../services/login.service';
import { ConfirmedValidator } from '../../helpers/confirmed-validator';
import { EncryptService } from '../../../mobile/services/encrypt.service';

@Component({
  selector: 'app-pass-change',
  templateUrl: './pass-change.component.html',
  styleUrls: ['./pass-change.component.css'],
  providers: [MessageService]
})
export class PassChangeComponent implements OnInit {

  changePassForm: FormGroup;
  submitted: boolean;
  msgs: Message[] = [];
  user: any;
  returnUrl: string;

  constructor(private msg: MessageService, private fb: FormBuilder, private router: Router,
              private loginService: LoginService, private encryptService: EncryptService) { }

  ngOnInit(): void {
    // Lay thong tin user tu form dang nhap
    this.user = JSON.parse(localStorage.getItem('user'));
    this.returnUrl = '';
    this.loginService.logout();

    // Khoi tao rang buoc form
    this.changePassForm = this.fb.group({
      newPassword: new FormControl('', [
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
      ])
    }, {
      validator: ConfirmedValidator('currentPassword', 'newPassword'),
      validator1: ConfirmedValidator('newPassword', 'confirmPassword')
    });
  }

  get f(): any { return this.changePassForm.controls; }

  changePassword(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.changePassForm.invalid) {
      return;
    }

    try {
      const param = {
        USER_NAME: this.user.USER_NAME,
        NEW_PASSWORD: this.encryptService.set('123$#@$^@1ERF', this.f.newPassword.value)
      };

      // kiem tra dang nhap
      this.loginService.changePassword(param).subscribe((response: any) => {
        if (response) {
          if (response.TYPE) {
            this.msgs = [];
            // tslint:disable-next-line:max-line-length
            this.msgs.push({ severity: 'error', summary: 'Error', detail: response.MESSAGE });
            this.f.newPassword.setValue('');
            this.f.confirmPassword.setValue('');
          } else {
            this.loginService.login(response);
            this.router.navigate([this.returnUrl]);
          }
        }
      }, err => {
        this.msgs = [];
        // tslint:disable-next-line:max-line-length
        this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Error.' });
        this.f.newPassword.setValue('');
        this.f.confirmPassword.setValue('');
      });
    } catch (error) {
      this.msgs = [];
      // tslint:disable-next-line:max-line-length
      this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Sorry, your username and password are incorrect - please try again.' });
      this.f.newPassword.setValue('');
      this.f.confirmPassword.setValue('');
    }
  }

}
