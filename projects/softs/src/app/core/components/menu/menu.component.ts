import { Component, OnInit } from '@angular/core';
import { MainComponent } from '../../../core/components/main/main.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  model: any[];

  constructor(public app: MainComponent) { }

  ngOnInit(): void {
    const user = localStorage.getItem('MADVI_KD');
    if (user && localStorage.getItem('isLoggedIn') === 'true') {
      this.model = [
        {
          label: 'Quản trị hệ thống', icon: 'pi pi-fw pi-mobile', routerLink: ['/usermobile'], badgeStyleClass: 'teal-badge',
          items: [
            { label: 'Quản trị thiết bị', icon: 'pi pi-fw pi-apple', routerLink: ['/usermobile/devices'] },
            { label: 'Quản trị tài khoản', icon: 'pi pi-fw pi-users', routerLink: ['/usermobile/accounts'] },
          ]
        },
        {
          label: 'Quản lý công việc', icon: 'pi pi-fw pi-mobile', routerLink: ['/task'], badgeStyleClass: 'teal-badge',
          items: [
            { label: 'Công việc hằng ngày', icon: 'pi pi-fw pi-apple', routerLink: ['/task/list'] },
            { label: 'Tổng hợp vật tự', icon: 'pi pi-fw pi-apple', routerLink: ['/task/report-vattu'] },
        
          ]
        },
        {
          label: 'Quyết toán vật tư', icon: 'pi pi-fw pi-mobile', routerLink: ['/qtoan'], badgeStyleClass: 'teal-badge',
          items: [
            { label: 'Danh sách quyết toán', icon: 'pi pi-fw pi-apple', routerLink: ['/qtoan/list'] },
           
        
          ]
        }
      ]
    }
    // this.model = [
    //   {
    //     label: 'Quản trị hệ thống', icon: 'pi pi-fw pi-mobile', routerLink: ['/usermobile'], badgeStyleClass: 'teal-badge',
    //     items: [
    //       { label: 'Quản trị thiết bị', icon: 'pi pi-fw pi-apple', routerLink: ['/usermobile/devices'] },
    //       { label: 'Quản trị tài khoản', icon: 'pi pi-fw pi-users', routerLink: ['/usermobile/accounts'] },
    //     ]
    //   },
    //   {
    //     label: 'Điều tra TTKH', icon: 'pi pi-fw pi-id-card', routerLink: ['/dtttkhmobile'], badgeStyleClass: 'teal-badge',
    //     items: [
    //       { label: 'Giao biên bản', icon: 'pi pi-fw pi-download', routerLink: ['/dtttkhmobile/bbangiao'] },
    //       { label: 'Tìm biên bản giao', icon: 'pi pi-fw pi-download', routerLink: ['/dtttkhmobile/searchbbanggiao'] },
    //       { label: 'Nhận biên bản', icon: 'pi pi-fw pi-upload', routerLink: ['/dtttkhmobile/bbannhan'] },
    //       { label: 'Tìm biên bản nhận', icon: 'pi pi-fw pi-download', routerLink: ['/dtttkhmobile/searchbbangnhan'] }
    //     ]
    //   }
    // ];
  }

  onMenuClick(): void {
    this.app.menuClick = true;
  }
}
