import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { LoginService } from '../../services/login.service';
import { ConfirmedValidator } from '../../helpers/confirmed-validator';
import { EncryptService } from '../../../mobile/services/encrypt.service';

@Component({
  selector: 'app-reset-passwrod',
  templateUrl: './reset-passwrod.component.html',
  styleUrls: ['./reset-passwrod.component.css'],
  providers: [MessageService]
})
export class ResetPasswrodComponent implements OnInit {

  changePassForm: FormGroup;
  submitted: boolean;
  msgs: Message[] = [];
  user: any;
  returnUrl: string;
  timeExp: any;
  constructor(private msg: MessageService, private fb: FormBuilder, private router: Router,private route: ActivatedRoute,
              private loginService: LoginService, private encryptService: EncryptService) { }

  ngOnInit(): void {
    // Lay thong tin user tu form dang nhap
    this.user = JSON.parse(localStorage.getItem('user'));
    this.returnUrl = '';
    this.loginService.logout();

    this.route.params.subscribe((params: any) => {
      this.user = params.id
      this.timeExp = params.time
    });
    // Khoi tao rang buoc form
    this.changePassForm = this.fb.group({
      newPassword: new FormControl('', [
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
      ])
    }, {
      validator: ConfirmedValidator('currentPassword', 'newPassword'),
      validator1: ConfirmedValidator('newPassword', 'confirmPassword')
    });
  }

  get f(): any { return this.changePassForm.controls; }

  changePassword(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.changePassForm.invalid) {
      return;
    }
    if(new Date().getTime() - Number.parseInt(this.timeExp) >= 216000000) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Đường link của bạn đã hết hạn' });
      return 
    }
    
    try {
      const param = {
        USER_NAME: this.user,
        NEW_PASSWORD: this.encryptService.set('123$#@$^@1ERF', this.f.newPassword.value)
      };
      // kiem tra dang nhap
      this.loginService.changePassword(param).subscribe((response: any) => {
        if (response) {
          if (response.TYPE) {
            this.msgs = [];
            // tslint:disable-next-line:max-line-length
            this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
            this.f.newPassword.setValue('');
            this.f.confirmPassword.setValue('');
          } else {
            this.router.navigate([this.returnUrl]);
          }
        }
      }, err => {
        this.msgs = [];
        // tslint:disable-next-line:max-line-length
        this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
        this.f.newPassword.setValue('');
        this.f.confirmPassword.setValue('');
      });
    } catch (error) {
      this.msgs = [];
      // tslint:disable-next-line:max-line-length
      this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
      this.f.newPassword.setValue('');
      this.f.confirmPassword.setValue('');
    }
  }

}
