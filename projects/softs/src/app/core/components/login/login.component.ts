import { Component, OnInit } from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { Router,ActivatedRoute } from '@angular/router';

import { LoginService } from '../../services/login.service';
import { EncryptService  } from '../../../mobile/services/encrypt.service';
import { AuthService } from '../../services/auth.service';
let ev = `o1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPY`
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  submitted: boolean;

  userName: string;

  password: string;

  returnUrl: string;

  msgs: Message[] = [];

  employId: any;
  token: any;
  constructor(private msg: MessageService,
    private route: ActivatedRoute, 
    private router: Router, 
    private loginService: LoginService, 
    private _encrypt: EncryptService,
    private _auth: AuthService
    ) { }

  ngOnInit(): void {
    this.returnUrl = '';
    this.loginToken();
    this.loginService.logout();
    console.log('user:',22120)
    console.log('password', 'Nhonnt@1234')
  }
  tokenGen(param:any): void {
    this.loginService.tokenLogin(param)
      .subscribe(
        (data) => {
          if(data.EMPLOYEE_ID) {
            this.employId = data.EMPLOYEE_ID
          } else {
            this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Tài khoản không hợp lệ vui lòng đăng nhập bằng tài khoản chương trình.' });
            this.router.navigate(['/']);
          }
          
        },
        (err) => {
          console.log(err)
        }
      );
    
  }
  loginToken():void {
    this.route.queryParams.subscribe(
      (query) => {
        if(query.token) {
          const param = {
            TOKEN: query.token
          }
          this.tokenGen(param)
        }
      },
      (err) => {
        this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Thông tin đăng nhập không chính xác - vui lòng thử lại.' });
      }
    )
  }
  login(): void {
    
    this.submitted = true;
    if (this.userName.trim() !== '' && this.password.trim() !== '') {
      try {
        const pLogin = {
          USER_NAME: this.userName.trim(),
          PASSWORD: this._encrypt.set(ev,this.password.trim())
        };
        // kiem tra dang nhap
        this.loginService.checkLogin(pLogin).subscribe((resLogin: any) => {
          if (resLogin) {
            if (resLogin.TYPE) {
              this.msgs = [];
              this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Thông tin đăng nhập không chính xác - vui lòng thử lại.' });
              this.userName = null;
              this.password = null;
            } else {
              const pAtt = {
                USER_ID: resLogin.USER_ID,
                ATTRIBUTE_NAME: 'requiresPasswordChange'
              };
              this.loginService.login(resLogin);
            }
          }
        });
      } catch (error) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Sorry, your username and password are incorrect - please try again.' });
      }
    }
  }
}
