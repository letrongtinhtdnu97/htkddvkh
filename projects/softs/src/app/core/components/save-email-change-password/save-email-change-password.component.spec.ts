import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveEmailChangePasswordComponent } from './save-email-change-password.component';

describe('SaveEmailChangePasswordComponent', () => {
  let component: SaveEmailChangePasswordComponent;
  let fixture: ComponentFixture<SaveEmailChangePasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveEmailChangePasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveEmailChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
