import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Message, MessageService } from 'primeng/api';
@Component({
  selector: 'app-save-email-change-password',
  templateUrl: './save-email-change-password.component.html',
  styleUrls: ['./save-email-change-password.component.css'],
  providers: [MessageService]
})
export class SaveEmailChangePasswordComponent implements OnInit {
  email: string
  userName: string
  submitted: boolean = false
  message: string = ''
  isButton: boolean
  msgs: Message[] = []
  constructor(private authService: AuthService,private msg: MessageService) {  }

  ngOnInit(): void {
    this.isButton = false
  }
  validateEmail(email: string): any {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  sendMail(mail: string, user: string, name: string):void {
    console.log(window.location)
    const param = {
      HOST: window.location.origin,
      MAIL: mail,
      USER: user,
      NAME: name ? name : ''
    }
    this.authService.sendMail(param)
      .subscribe(
        (data: any) => {
          console.log('data',data)
          if(data.MESSAGE === 'SUCCESS' && data.TYPE === 'SUCCESS') {
            this.isButton = true
            this.msgs = [];
            this.msgs.push({ severity: 'success', summary: 'Thông báo', detail: 'Vui lòng đăng nhập mail của bạn để thay đổi mật khẩu.' });
            return;
          }
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
            return
        },
        (err: any) => {
          this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
            return
        }
      );
  }
  send(): void {
    if(!this.userName) {      
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Vui lòng nhập thông tin tài khoản đăng nhập.' });
      return;
    }
    const param = {
      USER_NAME: this.userName,
    }
    this.authService.getUserByUserName(param)
      .subscribe(
        (data: any) => {
          if(data && data.email) {
            this.sendMail(data.email, this.userName,data.fullName)
            return
          }
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Không có tài khoản này trên hệ thống vui lòng kiểm tra lại.' });
          return;
        },
        (err: any) => {
          this.msgs = [];
          this.msgs.push({ severity: 'error', summary: 'Thông báo', detail: 'Lỗi vui lòng thử lại sau.' });
          return;
        }
      );
    
  }
}
