import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service'
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  numberRole: any;
  roles: any[] = [];
  constructor(private router: Router, private _auth: AuthService) { }

  canActivate(
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.roles = route.data["roles"] as Array<string> || [];

    
   
    if (this.isLoggedIn()) {
      const str = this.isLoggedIn()
      switch (str.length) {
        case 2:
          this.numberRole = 10
          break;
        case 4:
          this.numberRole = 70
          break;
        case 3:
          this.numberRole = 70
          break;
        case 6:
          this.numberRole = 99
          break;
        default:
          this.numberRole = 0
          break;
      }
      if(this.roles.includes(this.numberRole)) {
        return true;
      }
      
      this.router.navigate(['/notfound']);
      return false;
    }
    
    this.router.navigate(['/login']);
    return false;
  }

  public isLoggedIn(): any {
    const user = JSON.parse(localStorage.getItem('user'));
    const maDv = localStorage.getItem('MADVI_KD');
    if(localStorage.getItem('isLoggedIn') === 'true' && user?.ORG_CODE && maDv ) {
      return maDv
    } 
    return false
   
  }
}
