import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EncryptService } from '../../mobile/services/encrypt.service'
import { ConfigService } from '../../config/services/config.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
@Injectable()
export class LoginService {

  protected url = ConfigService.config.url;

  constructor(private http: HttpClient, private _auth: AuthService, private router: Router ) { }

  // checkLogin(param: any): any {
  //   return this.http.post(this.url.qtht + 'checkLogin', param);
  // }
  checkLogin(param: any): any {
    return this.http.post(this.url.qtht + 'checkUserLogin', param);
  }
  tokenLogin(param: any): any {
    return this.http.post(this.url.dtttkh + 'getEmpToken', param);
  }
  changePassword(param: any): any {
    return this.http.post(this.url.qtht + 'changePassword', param);
  }
  getEmployeeByID(param: any): any {
    return this.http.post(this.url.hr + 'getEmployeeByID', param);
  }
  login(user: any): void {
    localStorage.setItem('isLoggedIn', 'true');
    localStorage.setItem('user', JSON.stringify(user));
    const param = {
      ORG_CODE: user?.ORG_CODE,
      EMPLOYEE_ID: user?.EMPLOYEE_ID
    }
    this.getEmployeeByID(param).subscribe(
      (data) => {
        localStorage.setItem("MADVI_KD",data?.MADVI_KD)
        this.router.navigate(['']);
      },
      (err) => {
        console.log(err)
      }
    )
  }

  logout(): void {
    localStorage.setItem('isLoggedIn', 'false');
    localStorage.removeItem('user');
    localStorage.removeItem('MADVI_KD');
  }
}
