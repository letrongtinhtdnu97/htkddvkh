import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';


import { ConfigService } from '../../config/services/config.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    protected url = ConfigService.config.url;
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    login(param: any) {
        return this.http.post(this.url.qtht + 'checkLogin', param)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('isLoggedIn', 'true');
                    localStorage.setItem('user', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.setItem('isLoggedIn', 'false');
        localStorage.removeItem('user');
        localStorage.removeItem('MADVI_KD');
        this.currentUserSubject.next(null);
    }

    sendMail(param: any): any {
        return this.http.post(this.url.dtttkh + 'changePassword', param)
    }
    getEmployeeByID(param: any): any {
        return this.http.post(this.url.hr + 'getEmployeeByID', param);
    }
    getUserByUserName(param: any):any {
        return this.http.post(this.url.qtht + 'getUserByUserName', param)
    }
}