import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app.routes';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { ShareModule } from './Share/share.module';
import { MobileModule } from './mobile/mobile.module';
import { ConfigService } from '../app/config/services/config.service';
import { BnNgIdleService } from 'bn-ng-idle';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {JwtInterceptor} from './core/helpers/jwt.interceptor';
import {ErrorInterceptor} from './core/helpers/error.interceptor';
// tslint:disable-next-line:typedef
export function initializeApp(configService: ConfigService) {
    return (): Promise<any> => {
        return configService.load();
    };
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        CoreModule,

        ShareModule,
        MobileModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        BnNgIdleService,
        ConfigService,
        { 
            provide: APP_INITIALIZER, 
            useFactory: initializeApp, 
            deps: [ConfigService], 
            multi: true 
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
