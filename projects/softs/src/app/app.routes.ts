import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './core/guards/auth.guard';

import { MainComponent } from './core/components/main/main.component';
import { LoginComponent } from './core/components/login/login.component';
import { PassChangeComponent } from './core/components/pass-change/pass-change.component';
import { AccessdeniedComponent } from './core/pages/accessdenied/accessdenied.component';
import { NotfoundComponent } from './core/pages/notfound/notfound.component';
import { ErrorComponent } from './core/pages/error/error.component';
import { SaveEmailChangePasswordComponent } from './core/components/save-email-change-password/save-email-change-password.component';
import { ResetPasswrodComponent } from './core/components/reset-passwrod/reset-passwrod.component';

import { DashboardComponent } from './demo/view/dashboard.component';

import { DeviceListComponent } from './mobile/components/device-list/device-list.component';
import { AccountListComponent } from './mobile/components/account-list/account-list.component';
import { BbCmisListComponent } from './mobile/components/bb-cmis-list/bb-cmis-list.component';
import { BbCmisAddComponent } from './mobile/components/bb-cmis-add/bb-cmis-add.component';
import { BbCmisEditComponent } from './mobile/components/bb-cmis-edit/bb-cmis-edit.component';
import { BaoCaoTongHopVatTuComponent } from './mobile/components/bao-cao-tong-hop-vat-tu/bao-cao-tong-hop-vat-tu.component';
import { TaoBienBanComponent } from './mobile/components/tao-bien-ban/tao-bien-ban.component';
import { QuyettoanVattuComponent } from './mobile/components/quyettoan-vattu/quyettoan-vattu.component';





@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: MainComponent,
                children: [
                    { path: '', component: AccountListComponent, canActivate: [AuthGuard], data: {roles: [99]} },

                    { path: 'usermobile/accounts', component: AccountListComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'usermobile/devices', component: DeviceListComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'task/list', component: BbCmisListComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'task/add', component: BbCmisAddComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'task/edit/:id', component: BbCmisEditComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'task/report-vattu', component: BaoCaoTongHopVatTuComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'qtoan/list', component: QuyettoanVattuComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    { path: 'qtoan/create', component: TaoBienBanComponent, canActivate: [AuthGuard],data: {roles: [99]} },
                    
                ]
            },
            { path: 'error', component: ErrorComponent },
            { path: 'access', component: AccessdeniedComponent },
            { path: 'notfound', component: NotfoundComponent },
            { path: 'login', component: LoginComponent },
            { path: 'changepass', component: PassChangeComponent },
            { path: 'sendmail', component: SaveEmailChangePasswordComponent},
            { path: 'reset/:id/:time', component: ResetPasswrodComponent},
            { path: '**', redirectTo: '/notfound' },
        ], { scrollPositionRestoration: 'enabled' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
